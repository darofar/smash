package lsint.e4.smarthome;
/**
 * 
 * 
 * @author Nayeli  (nn.cases@alumnos.upm.es)
 */
public class Light extends Appliance {
	public Light(ApplianceState state, double onConsumption, double offConsumption, double standByConsumption, double totalConsumption) {
		super(state, onConsumption, offConsumption, offConsumption, totalConsumption, Light.class);
	}
}
