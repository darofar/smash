package lsint.e4.smarthome;


/**
 * 
 * 
 * @author Nayeli  (nn.cases@alumnos.upm.es)
 */
public class Television extends Appliance {
	
	public Television(ApplianceState state, double onConsumption, double offConsumption, double standByConsumption, double totalConsumption) {
		super(state, onConsumption, offConsumption, offConsumption, totalConsumption, Television.class);
	}
}
