package lsint.e4.smarthome;

import java.util.logging.Logger;

import lsint.e4.smash.action.ActuatorAgent;
import lsint.e4.smash.sensor.SensorAgent;
import lsint.e4.util.SmashLogger;

/**
 * 
 * 
 * @author Nayeli  (nn.cases@alumnos.upm.es)
 */
@SuppressWarnings("rawtypes")
public class Appliance {

	@SuppressWarnings("unused")
	private static Logger log = SmashLogger.getLogger(Appliance.class.getSimpleName());
	
	private String id;
	private static int counterID;
	private Class type;
	private double onConsumption;
	private double offConsumption;
	private double standByConsumption;
	private ApplianceState state;
	private double totalConsumption;
	private SensorAgent asociatedSensorAgent;
	private ActuatorAgent asociatedActuatorAgent;
	private String location;

	public enum ApplianceState {
		ON, STAND_BY, OFF
	}

	public Appliance(ApplianceState state2, double onConsumption, 
					double offConsumption, double standByConsumption, 
					double totalConsumption, Class type) {
		super();
		this.state = state2;
		this.onConsumption = onConsumption;
		this.offConsumption = offConsumption;
		this.standByConsumption = standByConsumption;
		this.totalConsumption = totalConsumption;
		this.id = Appliance.generateID();
		this.setType(type);
	}
	
	public static String generateID() {
		return "APPLIANCE#"+Appliance.counterID++;
	}
	public String getId() {
		return id;
	}

	public ApplianceState getState() {
		return state;
	}

	public void setState(ApplianceState state) {
		this.state = state;
	}


	public double getOnConsumption() {
		return onConsumption;
	}


	public void setOnConsumption(double onConsumption) {
		this.onConsumption = onConsumption;
	}


	public double getOffConsumption() {
		return offConsumption;
	}

	public void setOffConsumption(double offConsumption) {
		this.offConsumption = offConsumption;
	}


	


	public double getStandByConsumption() {
		return standByConsumption;
	}


	public void setStandByConsumption(double standByConsumption) {
		this.standByConsumption = standByConsumption;
	}

	public void changeState(){
		if(this.getState()== ApplianceState.OFF){
			this.setState(ApplianceState.ON);
		}else if(this.getState() == ApplianceState.ON){
			this.setState(ApplianceState.STAND_BY);
		}else{
			this.setState(ApplianceState.OFF);
		}
	}
	public double getTotalConsumption() {
		return totalConsumption;
	}
	public void setTotalConsumption(double totalConsumption) {
		this.totalConsumption = totalConsumption;
	}
	public Class getType() {
		return type;
	}
	public void setType(Class type) {
		this.type = type;
	}
	public SensorAgent getAsociatedSensorAgent() {
		return asociatedSensorAgent;
	}
	public void setAsociatedSensorAgent(SensorAgent asociatedSensorAgent) {
		this.asociatedSensorAgent = asociatedSensorAgent;
	}
	public ActuatorAgent getAsociatedActuatorAgent() {
		return asociatedActuatorAgent;
	}
	public void setAsociatedActuatorAgent(ActuatorAgent asociatedActuatorAgent) {
		this.asociatedActuatorAgent = asociatedActuatorAgent;
	}

	public Double getCurrentConsumption() {
		if(this.getState() == ApplianceState.ON){
			return this.getOnConsumption();
		}else if(this.getState() == ApplianceState.STAND_BY){
			return this.getStandByConsumption();
		}else{
			return this.getOffConsumption();
		}
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
