package lsint.e4.smarthome;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;

/**
 * 
 * 
 * @author Nayeli  (nn.cases@alumnos.upm.es)
 */
public class Person implements Steppable{

    private static final long serialVersionUID = 1;
    
    @SuppressWarnings("unused")
	private static Logger log = SmashLogger.getLogger(Person.class.getSimpleName());
    
	//Orden en el que se ejecuta este Steppable en la simulación.
	//Cuanto mas bajo antes se ejecuta. Ver documento SECUENCIA_EJECUCION.doc
	public static final int SEQUENCE_ORDER = 3;
	
    /*
     * Probabilidades de cambiar el estado de los siguientes elementos
     */
    private double probTV;
    private static int counterID;
	private double probFridge;
    private double probGeneralApp;
    private double probLight;
    private double probTher;
    private String id;
    /*
     * Parámetros adicionales
     */
    private Room room;
    

    private double probStayInRoom=0.20;
    
 
    public Person(double probTV, double probFridge, double probGeneralApp,
			double probLight, double probTher, Room room) {
		super();
		this.id=Person.generateID();
		this.probTV = probTV;
		this.probFridge = probFridge;
		this.probGeneralApp = probGeneralApp;
		this.probLight = probLight;
		this.probTher = probTher;
		
		this.room = room;
	}
    

    public static String generateID() {
        return "PERSON#"+Person.counterID++;
    }
    public String getId() {
        return this.id;
    }

	public double getProbTV() {
		return probTV;
	}

	public void setProbTV(double probTV) {
		this.probTV = probTV;
	}



	public double getProbFridge() {
		return probFridge;
	}



	public void setProbFridge(double probFridge) {
		this.probFridge = probFridge;
	}



	public double getProbGeneralApp() {
		return probGeneralApp;
	}



	public void setProbGeneralApp(double probGeneralApp) {
		this.probGeneralApp = probGeneralApp;
	}



	public double getProbLight() {
		return probLight;
	}



	public void setProbLight(double probLight) {
		this.probLight = probLight;
	}



	public double getProbClim() {
		return probTher;
	}



	public void setProbTher(double probTher) {
		this.probTher = probTher;
	}


	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	   
	@Override
	public void step(SimState arg0) {
		
		
		ArrayList<Room> room = SmashSimulation.rooms;
		//ArrayList<Person> p = SmashSimulation.peopleObj;
		Random r = new Random();
		double probRoom = r.nextDouble();
		if(!(probRoom < this.probStayInRoom)){			
			//double aux=this.probStayInRoom;
			boolean outLoop=true;
			for(int i=1;i<=room.size();i++){
				if((probRoom<(i*(1/(room.size())))) && outLoop){
					this.setRoom(room.get(i));		
					outLoop=false;
				}
			}
		}
		if(0.20<probRoom && probRoom<0.40){
			this.setRoom(room.get(0));				
		}else if(0.40<probRoom && probRoom<0.6){
			this.setRoom(room.get(1));
		}else if(0.6<probRoom && probRoom<0.9){
			this.setRoom(room.get(2));
		}else{
			this.setRoom(room.get(3));
		}
		double probApp = r.nextDouble();
		
		
		for(Appliance a : this.getRoom().getAppInRoom()){
			if(a instanceof Fridge){
				if(this.getProbFridge()>probApp){
					a.changeState();
					probApp=r.nextDouble();
				}
			}else if(a instanceof Thermostat){
				if(this.getProbTher()>probApp){
					a.changeState();
					probApp=r.nextDouble();
				}
			}else if(a instanceof Light){
				if(this.getProbLight()>probApp){
					a.changeState();
					probApp=r.nextDouble();
				}
			}else if(a instanceof Television){
				if(this.getProbTV()>probApp){
					a.changeState();
					probApp=r.nextDouble();
				}
			}else{
				if(this.getProbGeneralApp()>probApp){
					a.changeState();
					probApp=r.nextDouble();
				}
			}
		}

	
	}

	public double getProbTher() {
		return probTher;
	}

}
