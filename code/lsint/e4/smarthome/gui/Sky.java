package lsint.e4.smarthome.gui;

import java.util.logging.Logger;

import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;

public class Sky implements Steppable {
	
	private static Logger log = SmashLogger.getLogger(Sky.class.getSimpleName());

	
	 double skystep=0.001;
	 
	   public Sky(double skystep){
		   
		   this.skystep=skystep;
	   };

	
	
		private static final long serialVersionUID = 2;
		

		public void step(SimState state)
		{
			GUISetup.sky.add(skystep);
			if(GUISetup.sky.field[1][1]>1.0 || GUISetup.sky.field[1][1]<0.0){
				skystep=-skystep;
			}
		}
	}
	
	
	

