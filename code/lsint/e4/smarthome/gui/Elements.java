/* Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package lsint.e4.smarthome.gui;
import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.smarthome.Appliance;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.field.grid.DoubleGrid2D;

/** The element of a room */

public class Elements implements Steppable {
    private static final long serialVersionUID = 1;

    public enum State {
  	   ON, STAND_BY, OFF
  	 }
    
    public int state;
    public int room;
    //taille de la maison
    
    int size;
    //numero de la personne
    int number;
    int x;
    int y;
    Appliance app;
    
    private static Logger log = SmashLogger.getLogger(Elements.class.getSimpleName());
   
    void convertState(){
    	if(app.getState()==Appliance.ApplianceState.ON){
    		state=4;
    	}
    	if(app.getState()==Appliance.ApplianceState.OFF){
    		state=6;
    	}
    	if(app.getState()==Appliance.ApplianceState.STAND_BY){
    		state=5;
    	}
    }
    
    
    
    
    
    public Elements(int x,int y,Appliance app)
        {
    	state = 2;
    	this.x=x;
    	this.y=y;
    	this.app=app;
    
        
        }

   
    
    void locate(DoubleGrid2D grid){
    	int i;
    	int j;
    	for (i=-1;i<2;i++)
    		for(j=-1;j<2;j++){
    			grid.field[x+i][y+j]=state;
    		}
    		
    
    	}
    
    	
    
    	
    public void step(SimState state){
    	 SmashSimulation tut = (SmashSimulation)state;
    	 convertState();
    	 locate(GUISetup.electrics);
    }
    }