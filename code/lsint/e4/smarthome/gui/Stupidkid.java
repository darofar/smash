/* Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package lsint.e4.smarthome.gui;
import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.smarthome.Person;
import lsint.e4.smarthome.Room;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.field.grid.DoubleGrid2D;


/** Draw a person in the room it should be */

public class Stupidkid implements Steppable {
    private static final long serialVersionUID = 1;
    
    private static Logger log = SmashLogger.getLogger(Stupidkid.class.getSimpleName());

    Person pers;
    public int room;
    Room Room;
    
    //taille de la maison
    
    int size;
    //numero de la personne
    int number;
    int x;
    int y;
    public static final int[][] Kid = new int[][]
    	    {    {0, 2, 0, 3},
    	         {1, 2, 2, 0},
    	         {0, 2, 0, 3},
    	     };
    
    //size is the number of rooms!!
    public Stupidkid(int Room,int size,int number,Person per)
        {
   
    	this.size=size;
    	this.number=number;
    	this.pers=per;
        room = Room;
        }

   
    
   
   
    void DrawAkid (){
    	
    }
    
    void locate(DoubleGrid2D grid){
    	
    	int i=1;
    	int j=1;
    	int k=1;
    	int HouseSize=0;
    	
    	//il faut calculer les coordonnées en fonctions du nombre de rooms
    	 while(HouseSize*HouseSize < size){HouseSize++;}
    
    	 HouseSize++;
    	 int unit=100/(HouseSize);
    	//méthode barbare
    	 while(k< room){
    		 i++;
    		 if(i>size/2){
    			 i=1;
    			 j++;
    		 }
    		 k++;
    		 }
    	 x=i*unit+2*(number);
    	 y=j*unit+2*(number);
    	 System.out.println();
    		  for(i=0;i<Kid.length;i++)
    	            for(j=0;j<Kid[i].length;j++)
    	            { grid.field[x+i  - Kid.length/2]
    	                    [y+j - Kid[i].length/2] =
    	                    Kid[i][j];
    	            }
    		
    
    	}
    
void clear(DoubleGrid2D grid){
    	
    	int i=1;
    	int j=1;
    	int k=1;
    	int HouseSize=0;
    	
    	//il faut calculer les coordonnées en fonctions du nombre de rooms
    	 while(HouseSize*HouseSize < size){HouseSize++;}
    
    	 HouseSize++;
    	 int unit=100/(HouseSize);
    	//méthode barbare
    	 while(k< room){
    		 i++;
    		 if(i>size/2){
    			 i=1;
    			 j++;
    		 }
    		 k++;
    		 }
    	 x=i*unit+2*(number);
    	 y=j*unit+2*(number);
    	 System.out.println();
    		  for(i=0;i<Kid.length;i++)
    	            for(j=0;j<Kid[i].length;j++)
    	            { grid.field[x+i  - Kid.length/2]
    	                    [y+j - Kid[i].length/2] =
    	                    0;
    	            }
    		
    
    	}
    
    
    	
    	
    public void step(SimState state){
    	 SmashSimulation tut = (SmashSimulation)state;
    	 
    	 Room=pers.getRoom();
    	 
    	 if(room!= Room.getNum()+1){
    		 clear(GUISetup.electrics);
    		 room= Room.getNum()+1;
    	 
    	 
    	 }
    	 locate(GUISetup.electrics);
    	
     }
    }
