/* Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package lsint.e4.smarthome.gui;
import java.util.logging.Logger;

import lsint.e4.smarthome.Appliance;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.field.grid.DoubleGrid2D;

/** The Lights of a room(sorry for the s) */
public class Lights implements Steppable  {
	
	private static Logger log = SmashLogger.getLogger(Lights.class.getSimpleName());
    
	private static final long serialVersionUID = 1;
    public enum State { ON, STAND_BY, OFF }
    public int state;
    public int room;
    private int x;
    private int y;
    private int size;
    private Appliance app;
   
    void convertState(){
    	
    	if(app.getState()==Appliance.ApplianceState.ON){
    		state=2;
    	}
    	if(app.getState()==Appliance.ApplianceState.OFF){
    		state=0;
    	}
    	if(app.getState()==Appliance.ApplianceState.STAND_BY){
    		state=1;
    	}
    }
    
    public Lights(int x,int y,int size,Appliance app)
        {
    	this.x=x;
    	this.y=y;
    	this.size=size;
    	this.app=app;
    	state = 2;
        }

    public void changeState(int State){
    	state = State;
    }
    
    public int getRoom (){
    	return room; 
    }
    public int getState (){
    	return state; 
    }
    
    void DrawLights(DoubleGrid2D House){
    	
    	int s=state;
    	int i;
    	int j;
    	
    	for (i=x+2-size/2;i<=x-2+size/2;i++)
    		for(j=y+2-size/2;j<=y-2+size/2;j++){
    			House.field[i][j] = s;
    			
    		}
    	
    	
    	
    	}
    
    public void step(SimState state){
    	//SmashSimulation tut = (SmashSimulation)state;
    	convertState();
    	DrawLights(GUISetup.House);
     }
    }
