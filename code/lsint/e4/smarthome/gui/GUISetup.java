package lsint.e4.smarthome.gui;
import java.util.ArrayList;
import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.smarthome.Appliance;
import lsint.e4.smarthome.Light;
import lsint.e4.smarthome.Person;
import lsint.e4.smarthome.Room;
import lsint.e4.util.SmashLogger;
import sim.field.grid.DoubleGrid2D;
import sim.field.grid.SparseGrid2D;




public class GUISetup {
	
	private static Logger log = SmashLogger.getLogger(GUISetup.class.getSimpleName());

public static DoubleGrid2D sky;
public static DoubleGrid2D House;
public static DoubleGrid2D electrics;
public static SparseGrid2D particles;

public static int gridWidth = 100;
public static int gridHeight = 100;

static SmashSimulation simulation;
/* //the Lights
public Lights Lights1= new Lights(1);
public Lights Lights2= new Lights(2);
public Lights Lights3= new Lights(3);
public Lights Lights4= new Lights(4);
*/


static void DrawHouse(int rooms,ArrayList<Room> Rooms){
	
	ArrayList<Appliance> array;
	
	int i=0;
	
	
	int x=1;
	int y=1;
	int size=0;
	int taille=gridWidth;
	
	//taille d'un mur
	int mur=1;
	//obtenir la taille du carré: 
	 while(size*size < rooms){size++;}
	 //adapter au nombre de murs
	 size++;
	
	//l'unité de dessin
	 int unit=taille/(size);
while(i<Rooms.size()){
	array=Rooms.get(i).getAppInRoom();
  
	DrawSquare(x*unit,y*unit,unit,mur,House.field);

	//creer les agents de dessin associés à chaque objet.
	int l=0;
	int pos=0;
	int m=0;
	int[] x1={-1,-1,1,1};
	int[] x2={-1,1,-1,1};
	
	
while(l<array.size()){
	if(array.get(l) instanceof Light){
		
		
		Lights lights=new Lights(x*unit,y*unit,unit-1,array.get(l));
		simulation.schedule.scheduleRepeating(lights);
		l++;
		
	}else{
		Elements el=new Elements(x*unit+x1[pos]*(unit/2-mur*4),
								y*unit+x2[pos]*(unit/2-mur*4),
								array.get(l)
				);
		
		
		
		simulation.schedule.scheduleRepeating(el);
		l++;
		
		pos++;
		
		
	}
	
	
	
};
	
	
	
	x++;
	if(x>size-1){
		x=1;
		y++;
	}

	i++;
}

	
}

static void DrawSquare(int x,int y,int size,int mur,double[][] field)
{
	for(int i=x-size/2-mur; i<=x+size/2+mur;i++)
		for(int j=y-size/2-mur; j<=y+size/2+mur;j++){
			if((i>=x-size/2-mur && i<=x-size/2+mur) ||
					(i>=x+size/2-mur && i<=x+size/2+mur)){
				field[i][j]=3;
			}
			if((j>=y-size/2-mur && j<=y-size/2+mur)||
					(j>=y+size/2-mur && j<=y+size/2+mur)){
				field[i][j]=3;
			}
		}
		

}

		


static void DrawPerson(int numRoom,ArrayList<Person> array){
	int i=0;
	int room=1;
	Stupidkid pers;
	//Se define un numero a cada persona para que no se ponen en el mismo sitio
	while(i<array.size()){
		
		pers = new Stupidkid(room,numRoom,i,array.get(i));
		simulation.schedule.scheduleRepeating(pers);
		i++;
	}
}


public static void setup (SmashSimulation smashsimulation){
	simulation=smashsimulation;
	House = new DoubleGrid2D(gridWidth,gridHeight);
	sky = new DoubleGrid2D(gridWidth, gridHeight);
	electrics = new DoubleGrid2D(gridWidth, gridHeight);
	

	DrawHouse(simulation.NUMBER_OF_ROOMS,simulation.rooms);
    DrawPerson(simulation.NUMBER_OF_ROOMS,simulation.people);
    Sky sky=new Sky((double) simulation.MINS_PER_STEP*1/(60*12));
    simulation.schedule.scheduleRepeating(sky);

	
	
}



}
