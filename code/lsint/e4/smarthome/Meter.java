package lsint.e4.smarthome;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;

/**
 * Esta clase representa el contador electrico de la casa al que se le presupone
 * posee algo de interactividad, y es capaz de discernir el consumo de cada
 * electrodoméstico conectado a la red eléctrica. 
 * 
 * @author Nayeli (nn.cases@alumnos.upm.es)
 * @author Danny (rolando.fonseca.arboleda@alumnos.upm.es)
 * @version v0.1.1
 */
public class Meter implements Steppable{
	
	private static final long serialVersionUID = 6589479934622766035L;
	private static Logger log = SmashLogger.getLogger(Meter.class.getSimpleName());
	
	/**
	 * Orden en el que se ejecuta este Steppable en la simulación.
	 * Cuanto mas bajo antes se ejecuta. Ver documento SECUENCIA_EJECUCION.doc
	 */
	public static final int SEQUENCE_ORDER = 5;
	
	/**
	 * Identificadior único. 
	 */
	private String id;
	
	/**
	 * Colección que relaciona los identificadores de lo electrodomesticos con 
	 * el consumo que ha generado en el último paso de la simulación.  
	 */
	public HashMap<String, Double> consumptionPerAppliance = new HashMap<String, Double>();
	
	/**
	 * Colección que guarda una relación de los electrodomésticos y el consumo
	 * total que ha generado a lo largo de toda la simulación. 
	 */
	public HashMap<String, Double> totalConsumptionPerAppliance = new HashMap<String, Double>();

	/**
	 * Constructor por defecto. 
	 */
	public Meter() {
		this.id = "METER#0";
		log.log(Level.FINE, this.getId()+" - created!");
	}
	
	/**
	 * En cada paso de la simulación el Steppable Meter debe reunir toda la 
	 * información de los consumos actuales de los electrodométicos y guardarla
	 * en sus variable sinternas.
	 */
	@Override
	public void step(SimState arg0) {
		for (Room r : SmashSimulation.rooms) {
			for (Appliance app : r.getAppInRoom()) {
				this.consumptionPerAppliance.put(app.getId(), app.getCurrentConsumption());
				this.totalConsumptionPerAppliance.put(app.getId(), 
						app.getCurrentConsumption() + this.totalConsumptionPerAppliance.get(app.getId()));
			}
		}
	}
	
	/**
	 * Devuelve la colección que relaciona los electrodomésticos con el consumo
	 * en el último paso de la simulación. 
	 * @return consumptionPerAppliance - HashMap<ApplianceId, ConsumoPasoActual>
	 */
	public HashMap<String, Double> getConsumptionPerAppliance() {
		return this.consumptionPerAppliance;
	}
	
	/**
	 * Devuelve la colección que relaciona los electrodomésticos con el consumo
	 * acumulado durante toda la simulación.
	 * @return 
	 *   totalConsumptionPerAppliance - HashMap<ApplianceId, ConsumoTotalAcumulado>
	 */
	public HashMap<String, Double> getTotalConsumptionPerAppliance() {
		return this.totalConsumptionPerAppliance;
	}
	
	/**
	 * Calcula y devuelve una relación del consumo de la habitación en el paso 
	 * actual de la simulación. 
	 * @return roomsConsumption - HashMap<RoomID, ConsumoPasoActual>
	 */
	public HashMap<String, Double> getConsumptionPerRoom() {
		HashMap<String, Double> roomsConsumption = new HashMap<String, Double>();
		for (Room r : SmashSimulation.rooms) {
			roomsConsumption.put(r.getId(), 0.0);
			for (Appliance app : r.getAppInRoom()) {
				roomsConsumption.put(r.getId(), 
									app.getCurrentConsumption() + 
									roomsConsumption.get(r.getId()));
			}
		}
		return roomsConsumption;
	}
	
	/**
	 * Calcula y devuelve una relación del consumo de la habitación a lo largo 
	 * de toda la simulación. 
	 * @return roomsTotalConsumption - HashMap<RoomID, ConsumoTotalAcumulado>
	 */
	public HashMap<String, Double> getTotalConsumptionPerRoom() {
		HashMap<String, Double> roomsTotalConsumption = new HashMap<String, Double>();
		for (Room r : SmashSimulation.rooms) {
			roomsTotalConsumption.put(r.getId(), 0.0);
			for (Appliance app : r.getAppInRoom()) {
				roomsTotalConsumption.put(r.getId(), 
									this.getTotalConsumptionPerAppliance().get(app.getId())+ 
									roomsTotalConsumption.get(r.getId()));
			}
		}
		return roomsTotalConsumption;
	}
	
	/**
	 * Inicializa las colecciones de datos. Es IMPRESCINDIBLE que este metodo se
	 * llame despues de haber sido creadas las habitaicones y los electrodomésticos
	 * de la habitación.
	 */
	public void init(){
		for(int j=0;j<SmashSimulation.NUMBER_OF_ROOMS;j++){
			for(Appliance a : SmashSimulation.rooms.get(j).getAppInRoom()){
				this.consumptionPerAppliance.put(a.getId(), 0.0);
				this.totalConsumptionPerAppliance.put(a.getId(), 0.0);
			}
		}
	}
	
	/**
	 * Devuelve el identificador del Contador. 
	 * @return id - identificador unico METER#0
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Calcula y devuelve un valor que representa el consumo total de la casa 
	 * a lo largo de la simulación. 
	 * @return - total: Consumo total de la SmartHome en el tiempo de ejecución. 
	 */
	public double getTotalConsumption() {
		double total = 0;
		for (Double tmp : this.totalConsumptionPerAppliance.values()) {
			total += tmp;
		}
		return total;
	}

}
