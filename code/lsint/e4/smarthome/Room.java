package lsint.e4.smarthome;

import java.util.ArrayList;
import java.util.logging.Logger;

import lsint.e4.util.SmashLogger;

/**
 * 
 * 
 * @author Nayeli  (nn.cases@alumnos.upm.es)
 */
public class Room {
	
	@SuppressWarnings("unused")
	private static Logger log = SmashLogger.getLogger(Room.class.getSimpleName());
	
	private int num = 0;
	private ArrayList<Appliance> appInRoom = new ArrayList<Appliance>();
	private ArrayList<Person> peopleInRoom = new ArrayList<Person>();
	private String id = "";
	private static int counterID = 0;
	
	public static String generateID() {
	    return "ROOM#"+Room.counterID++;
	}
	public String getId() {
	    return id;
	}

	public Room(int num, ArrayList<Appliance> appInRoom) {
		super();
		this.num = num;
		this.appInRoom = appInRoom;
		this.id = Room.generateID();
	}
	
	public ArrayList<Appliance> getAppInRoom() {
		return this.appInRoom;
	}

	public void setAppInRoom(ArrayList<Appliance> appInRoom) {
		this.appInRoom = appInRoom;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public ArrayList<Person> getPeopleInRoom() {
		return peopleInRoom;
	}
	public void setPeopleInRoom(ArrayList<Person> peopleInRoom) {
		this.peopleInRoom = peopleInRoom;
	}
	public void addPeopleInRoom(Person p) {
		this.peopleInRoom.add(p);
	}
}
