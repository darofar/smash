package lsint.e4.smarthome;

/**
 * 
 * 
 * @author Nayeli  (nn.cases@alumnos.upm.es)
 */
public class Fridge extends Appliance{
	public Fridge(ApplianceState state, double onConsumption, double offConsumption, double standByConsumption, double totalConsumption) {
		super(state, onConsumption, offConsumption, offConsumption, totalConsumption, Fridge.class);
	}
}
