package lsint.e4.smarthome;

import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;
/**
 * 
 * 
 * @author Nayeli  (nn.cases@alumnos.upm.es)
 */
public class Ilumination implements Steppable{

	private static final long serialVersionUID = 7089057882331425090L;
	
	@SuppressWarnings("unused")
	private static Logger log = SmashLogger.getLogger(Ilumination.class.getSimpleName());
	
	/**
	 * Orden en el que se ejecuta este Steppable en la simulación.
	 * Cuanto mas bajo antes se ejecuta. Ver documento SECUENCIA_EJECUCION.doc
	 */
	public static final int SEQUENCE_ORDER = 1;
	
	/**
	 * identificador único. 
	 */
	private String id;
	private static int counterID;
	
	/**
	 * Contructor por defecto. 
	 */
	public Ilumination() {
		this.id = Ilumination.generateID();
	}

	@Override
	public void step(SimState arg0) {
		this.ilumination();
	}
	
	public void ilumination(){
		//Cada hora se incrementa/decrementa la iluminosidad global
		SmashSimulation.auxIlum= SmashSimulation.auxIlum+SmashSimulation.MINS_PER_STEP;
		if(SmashSimulation.auxIlum>=60){
			if(SmashSimulation.globalPercentajeIlumination>=100 || SmashSimulation.globalPercentajeIlumination<=0){
				SmashSimulation.incrIlum = -SmashSimulation.incrIlum;
			}
			
			SmashSimulation.globalPercentajeIlumination= SmashSimulation.globalPercentajeIlumination+SmashSimulation.incrIlum; 

		}else{
			SmashSimulation.auxIlum++;
		}
	}

	
	public static String generateID() {
	    return "ILUMINATION#"+Ilumination.counterID++;
	}
	public String getId() {
	    return id;
	}

}
