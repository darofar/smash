package lsint.e4.smash.sensor;

import java.util.logging.Logger;

import lsint.e4.smarthome.Appliance;
import lsint.e4.smarthome.Appliance.ApplianceState;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;

/**
 * This class represents the behaviour of a driver agent in the sensory label. 
 * As sensor the agent has to monitors the state of a given appliance, called 
 * his subject. In addition this agent has to be able to communicate this 
 * information to the context layer (or any other layer that ask for).
 * 
 * The communication can be handle in two ways. 
 * - When the agent detects a change in the subject state, it will inform the  
 *   Context layer.
 * - The Context layer can ask at any giving time for this information.  
 * 
 * @author Danny (darofar@gmail.com)
 *
 */
public class SensorAgent implements Steppable {
	
	private static final long serialVersionUID = -6263541809067678759L;
	
	//Orden en el que se ejecuta este Steppable en la simulación.
	//Cuanto mas bajo antes se ejecuta. Ver documento SECUENCIA_EJECUCION.doc
	public static final int SEQUENCE_ORDER = 4;
	
	private Appliance subject;
	private ApplianceState subjectState;
	private String id;
	private static int counterID;
	private static Logger log = SmashLogger.getLogger(SensorAgent.class.getSimpleName());
	
	public SensorAgent(Appliance subject){
		super();
		this.id = SensorAgent.generateID();
		this.subject = subject;
		this.subjectState = null;
		log.fine(this.id+" - created!");
		subject.setAsociatedSensorAgent(this);
	}
	
	private static String generateID() {
		return "SENSOR_AGENT#"+SensorAgent.counterID++;
	}

	public ApplianceState getSubjectState() {
		return this.subjectState;
	}
	
	public String getSubjectID() {
		return this.subject.getId();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public void step(SimState sim) {
		this.subjectState = this.subject.getState();
	}
}
