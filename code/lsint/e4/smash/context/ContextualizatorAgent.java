package lsint.e4.smash.context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.smarthome.Appliance;
import lsint.e4.smarthome.Appliance.ApplianceState;
import lsint.e4.smarthome.Meter;
import lsint.e4.smarthome.Person;
import lsint.e4.smarthome.Room;
import lsint.e4.smash.action.Action;
import lsint.e4.smash.action.ActuatorAgent;
import lsint.e4.smash.action.SchedulerAgent;
import lsint.e4.smash.reasoning.ReasoningAgent;
import lsint.e4.smash.sensor.SensorAgent;
import lsint.e4.smash.statistics.StatsAgent;
import lsint.e4.util.SmashLogger;

import org.json.JSONObject;

import sim.engine.SimState;
import sim.engine.Steppable;

/**
 * Este agente tiene como tarea reunir toda la información de la simulación con
 * el fin de crear el contexto de la casa inteligente. En base a este contexto 
 * se ejecutará el sistema experto que decidirá que acciones deben ser tomadas
 * para optimizar el consumo eléctrico de la simulación. 
 * El contexto también será entregado al agente encargado de las estadísticas 
 * que procederá a guardar la información del contexto en un fichero, para su
 * futuro análisis. 
 * 
 * @author Danny (darofar@gmail.com)
 * @version v0.1.0
 */
public class ContextualizatorAgent implements Steppable {

	private static final long serialVersionUID = 5208096050645986449L;
	
	/**
	 * Orden en el que se ejecuta este Steppable en la simulación.
	 * Cuanto mas bajo antes se ejecuta. Ver documento SECUENCIA_EJECUCION.doc
	 */
	public static final int SEQUENCE_ORDER = 6;
	
	private static Logger log = SmashLogger.getLogger(ContextualizatorAgent.class.getSimpleName());
	
	/**
	 * Objeto Contexto que será pasado a los agentes Razonador y Estadisticas. 
	 */
	private JSONObject context;
	
	//Objetos de referencia a elementos de la simulación. 
	private ArrayList<SensorAgent> sensors;
	private ArrayList<ActuatorAgent> actuators;
	@SuppressWarnings("unused")
	private SchedulerAgent scheduler;
	private ReasoningAgent reasoner;
	private StatsAgent statistics;
	private Meter meter;
	
	/**
	 * Identificador único del agente. 
	 */
	private String id;

	public ContextualizatorAgent(ReasoningAgent reasoner2,
			StatsAgent statistics2, SchedulerAgent scheduler2,
			ArrayList<SensorAgent> sensors2, 
			ArrayList<ActuatorAgent> actuators2, 
			Meter meter2) {
		this.reasoner = reasoner2;
		this.statistics = statistics2;
		this.scheduler = scheduler2;
		this.sensors = sensors2;
		this.actuators = actuators2;
		this.meter = meter2;
		this.id = "CONTEXTUALIZATOR#0";
		log.log(Level.FINE, this.getId()+" - created!");
	}

	/**
	 * En cada paso el agente Contextualizador debe recorrer todos los elementos
	 * de la simulación para obtener la información de su etsado, y en base a 
	 * esta información generar el objeto de contexto. 
	 */
	@Override
	public void step(SimState arg0) {
		SmashSimulation simulation = (SmashSimulation) arg0;
		this.makeContext(simulation);
		//Pasamos el contexto al agente Estadisticas. 
		this.statistics.setContext(this.context);
		//Pasamos el contexto al agente razonador. 
		this.reasoner.setContext(this.context);
	}

	/**
	 * Metodo utilitario que se encarga de generar el contexto propiamente dicho. 
	 * un ejemplo del objeto final puede verse en $SMASH_HOME/misc/context/context_example.json.
	 *  
	 * @param simulation - el objeto simulación. 
	 * @see $SMASH_HOME/misc/context/context_example.json.
	 */
	private void makeContext(SmashSimulation simulation) {
		this.context = new JSONObject();
		try {
			//Simulación 
			JSONObject jsonSimulation = new JSONObject();
			//jsonContext.put("simulation", jsonSimulation);	
				
			// Habitaciones.
			JSONObject jsonRooms = new JSONObject();
			jsonSimulation.put("rooms", jsonRooms);
			
			//Electrodomesticos
			JSONObject jsonAppliances = new JSONObject();
			jsonSimulation.put("appliances", jsonAppliances);
			
			//Personas
			JSONObject jsonPeople = new JSONObject();
			jsonSimulation.put("people", jsonPeople);
			
			//Agentes
			JSONObject jsonAgents = new JSONObject();
			jsonSimulation.put("agents", jsonAgents);
			
			//Acciones ??? Aun no se usa
			JSONObject jsonActions = new JSONObject();
			jsonSimulation.put("actions", jsonActions);
			
			//Consumo
			JSONObject jsonConsumption = new JSONObject();
			jsonSimulation.put("consumption", jsonConsumption);
			
			//Generar campos de la simulación
			//	"simulation": {
			//		"id" : "SIMULATION#XXX",
			//		"date": "01-01-2014_23-59-59",
			//		"step" : "13",
			//		"step_multiplier": "5",
			//		"total_rooms": "4",
			//		"total_appliance": "13",
			//		"total_persons" : "3",
			//	}
			jsonSimulation.put("id", simulation.getId());
			jsonSimulation.put("date", new SimpleDateFormat("YYYYMMddHHmmss").format(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime())));
			jsonSimulation.put("step", simulation.schedule.getSteps());
			jsonSimulation.put("step_multiplier", SmashSimulation.MINS_PER_STEP);
			jsonSimulation.put("total_rooms", SmashSimulation.NUMBER_OF_ROOMS);
			jsonSimulation.put("total_people", SmashSimulation.people.size());
			int totalAppliances = 0;
			for (Room room : SmashSimulation.rooms) {
				//Generar campos de cada habitación. 
				//Añadimos un objeto roomjson por cada habitación. 
				//"room#1": {
				//	"number_of_appliance": 2,
				//	"appliances" : ["appliance#3", "appliance#4", "appliance#1"],
				//	"number_of_persons" : 1,
				//	"persons" : ["person#1"]
				//}
				JSONObject tmpRoomJson = new JSONObject();
				jsonRooms.put(room.getId(), tmpRoomJson);
				tmpRoomJson.put("id", room.getId());
				tmpRoomJson.put("number_of_appliance", room.getAppInRoom().size());
				tmpRoomJson.put("number_of_persons", room.getPeopleInRoom().size());
				
				ArrayList<String> appsIds = new ArrayList<String>();
				//Aprovechamos el bucle de habitaciones para coger los electrodomesticos. 
				for (Appliance appliance : room.getAppInRoom()) {
					appsIds.add(appliance.getId());
					totalAppliances++;
					//Generar campos de cada electrodomestico.
					//	"appliance#1" : {
					//		"type": "Light",
					//		"location" : "room#1",
					//		"sensor" : "sensor#1",
					//		"actuator" : "actuator#1",
					//		"consumption" : {
					//			"disconnected" : "0",
					//			"active" : "100",
					//			"standby" : "20"
					//		} 
					//	}
					JSONObject tmpApplianceJson = new JSONObject();
					jsonAppliances.put(appliance.getId(), tmpApplianceJson);
					tmpApplianceJson.put("type", appliance.getClass().toString());
					tmpApplianceJson.put("location", room.getId());
					//System.out.println(appliance.getAsociatedSensorAgent());
					tmpApplianceJson.put("sensor", appliance.getAsociatedSensorAgent().getId());
					tmpApplianceJson.put("actuator", appliance.getAsociatedActuatorAgent().getId());
					JSONObject applianceConsumption = new JSONObject();
					tmpApplianceJson.put("consumption", applianceConsumption);
					applianceConsumption.put(ApplianceState.OFF.toString(), appliance.getOffConsumption());
					applianceConsumption.put(ApplianceState.ON.toString(), appliance.getOnConsumption());
					applianceConsumption.put(ApplianceState.STAND_BY.toString(), appliance.getStandByConsumption());
				}
				tmpRoomJson.put("appliances",appsIds);
				
				ArrayList<String> peopleIds = new ArrayList<String>();
				//Aprovechamos el bucle de habitaciones para coger las personas. 
				for (Person p : room.getPeopleInRoom()) {
					peopleIds.add(p.getId());
					//Generar campos de cada persona.
					//	"person#1" : {
					//		"location" : "room#1"
					//	}
					JSONObject tmpPersonJson = new JSONObject();
					jsonPeople.put(p.getId(), tmpPersonJson);
					tmpPersonJson.put("location", room.getId());
				}
				tmpRoomJson.put("persons", peopleIds);
			}
			jsonSimulation.put("total_appliance", totalAppliances); 
			
			//Generar campos de la información de consumo.
			// "consumption" : {
			//	"total" : "150",
			//	"per_room" : {
			//		"room#1" : "39",
			//		"room#2" : "27",
			//		...
			//	},
			//	"per_appliance" : {
			//		"appliance#1" : "17",
			//		"appliance#2" : "4",
			//		...
			//	},
			//TODO esta parte aún no está implementada, no hay información de 
			// 		consumo por persona. 
			//	"per_person" : {
			//		"person#1" : "45",
			//		"person#2" : "67",
			//		...
			//	}
			// }
			jsonConsumption.put("total", this.meter.getTotalConsumption());
			JSONObject tmpPerRoom = new JSONObject();
			jsonConsumption.put("per_room", tmpPerRoom);
			for (Map.Entry<String, Double> entry: this.meter.getTotalConsumptionPerAppliance().entrySet()) {
				tmpPerRoom.put(entry.getKey(), entry.getValue());
			}
			JSONObject tmpPerAppliance = new JSONObject();
			jsonConsumption.put("per_appliance", tmpPerAppliance);
			for (Map.Entry<String, Double> entry: this.meter.getTotalConsumptionPerAppliance().entrySet()) {
				tmpPerAppliance.put(entry.getKey(), entry.getValue());
			}
			
			//Generando campos de agentes.
			//"agents" : {
			//	"reasoning" : {
			//		"id": "reasoning0",
			//		"state" : "???"
			//	}, 
			//	"scheduler" : {
			//		"id" : "scheduler#0", 
			//		"current_plans" : ["plan#0"],
			//		"queue_orders" : ["action#0", "action#1"]
			//	}
			//}
			
			//TODO cuando este hecho el razonador, extraer info útil aquí. 
			//JSONObject jsonReasoningAgent = new JSONObject();
			//jsonReasoningAgent.put("id", this.reasoner.getID());
			//jsonAgents.put("reasoner", jsonReasoningAgent);
			
			//TODO cuando este hecho el planificador, extraer info útil aquí.
			//JSONObject jsonSchedulerAgent = new JSONObject();
			//jsonAgents.put("scheduler", jsonSchedulerAgent);
			//jsonSchedulerAgent.put("id", this.reasoner.getID());
			//jsonSchedulerAgent.put("current_plans", this.scheduler.getPlanList());
			//jsonSchedulerAgent.put("queue_orders", this.scheduler.getQueueOrders());
			
			JSONObject jsonSensors = new JSONObject();
			jsonAgents.put("sensors", jsonSensors);
			for (SensorAgent sensor : this.sensors) {
				// "sensors" : {
				//		"sensor0" : {
				//			"subject" : "appliance#0",
				//			"subjectState" : "ON"
				//		}, 
				//		...
				//	},
				JSONObject tmpSensorJson = new JSONObject();
				jsonSensors.put(sensor.getId(), tmpSensorJson);
				tmpSensorJson.put("id", sensor.getId());
				tmpSensorJson.put("subject", sensor.getSubjectID());
				tmpSensorJson.put("subject_state", sensor.getSubjectState().toString());
			}
			
			JSONObject jsonActuators = new JSONObject();
			jsonAgents.put("actuators", jsonActuators);
			for (ActuatorAgent actuator : this.actuators) {
				// "actuators" : {
				//		"actuator0" : {
				//			"id" : "actuator#0"
				//			"subject" : "appliance#0",
				//			"pending-orders" : ["action#0", "action#1"]					
				//		},
				//		...
				// },
				JSONObject tmpActuatorJson = new JSONObject();
				jsonActuators.put(actuator.getId(), tmpActuatorJson);
				tmpActuatorJson.put("id", actuator.getId());
				tmpActuatorJson.put("subject", actuator.getSubjectID());
				tmpActuatorJson.put("pending_orders", actuator.getOrdersAsString());
				
				for (Action action : actuator.getOrders()) {
					//"actions" : {
					//"action1" : {
					//	"id" : "action#1",
					//	"target" : "appliance#0",
					//	"type" : "CHANGE_STATE",
					//	"value" : "OFF",
					//	}
					//}
					JSONObject tmpActionJson = new JSONObject();
					jsonActions.put(action.getId(), tmpActionJson);
					tmpActionJson.put("id", action.getId());
					tmpActionJson.put("target", action.getTarget());
					tmpActionJson.put("type", action.getActionType().toString());
					tmpActionJson.put("value", action.getValue().toString());
				}
			}
			this.context.put("simulation", jsonSimulation);
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public JSONObject getContext() {
		return context;
	}

	public String getId() {
		return id;
	}

	public void setSensors(ArrayList<SensorAgent> sensors) {
		this.sensors = sensors;
	}
	
	public void addSensor(SensorAgent sa) {
		this.sensors.add(sa);
	}

	public void setActuators(ArrayList<ActuatorAgent> actuators) {
		this.actuators = actuators;
	}
	
	public void addActuators(ActuatorAgent agent) {
		this.actuators.add(agent);
	}

}
