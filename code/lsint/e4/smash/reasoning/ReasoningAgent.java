package lsint.e4.smash.reasoning;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Logger;

import jess.JessException;
import jess.Rete;
import lsint.e4.util.SmashLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sim.engine.SimState;
import sim.engine.Steppable;

/**
 * El agente razonador es un wraper de un sistema experto de reglas escrito en 
 * Jess. El objetivo de este sistema experto es decidir que acciones debe ejecutar 
 * SMASH! según los planes de ahorro que hayán sido configurados. 
 * 
 * Un plan de ahorro de consumo es una serie de reglas y acciones con el objetivo
 * de reducir el consumo. El agente razonador decide las acciones que deben ser 
 * ejecutadas aplicando las reglas definidas por el plan al estado actual de todos 
 * los elementos de la SmartHome, que obtiene a través del objeto contexto. 
 * 
 * Este agente ocupa la posición 8 en al secuencia de ejecución. 
 * 
 * @author Danny (darofar@gmail.com)
 * @author antoine ()
 *
 */
public class ReasoningAgent implements Steppable {

	private static final long serialVersionUID = -4220737447623613375L;

	//Logger de la clase
	private static Logger log = SmashLogger.getLogger(ReasoningAgent.class.getSimpleName());
	
	/**
	 * Identificador único. 
	 */
	private String id;
	
	/**
	 * Interfaz entre Java y Jess
	 */
	private Rete rete;
	
	private JSONObject order = new JSONObject();
	private JSONObject context = new JSONObject();
	private JSONObject fact = new JSONObject();
	
	public ReasoningAgent() {
		this.id = "REASONER#0";
		this.rete = new Rete();
		log.fine(this.id+" - created!");
	}

	/**
	 * Main para hacer pruebas sobre el plan escrito en Jess. 
	 */
public static void main(String[] args) throws Exception {
	
	ArrayList<String> jessAppliances;
	ArrayList<String> jessRooms;
	Rete jrete = new Rete();
	
	//Inicialización
	jrete.clear();
	
	//Cargar Planes. 
	String plan = (System.getProperty("user.dir").toString()+"/code/lsint/e4/smash/reasoning/jess/shutdownUnattended.clp");
	System.out.println(plan);
	try{
		//intentamos normal
		jrete.batch(plan);
	} catch (Exception fnfe) {
		//puede ser que windows la esteliando, intentamos windows. 
		jrete.batch(plan.replace("/", "\\"));
	}
	
	JSONObject appliances;
	JSONObject rooms;
	JSONObject sensors;
	JSONObject context;
	byte[] encoded;
	String jsonRaw;
	try {
		encoded = Files.readAllBytes(Paths.get(System.getProperty("user.dir").toString()+"/misc/context/simulation_0603194153/context_simulation_0603194153_step200.json"));
		jsonRaw = new String(encoded, "UTF8");
		context = new JSONObject(jsonRaw).getJSONObject("simulation");
		//System.out.println(context.toString());
		
		appliances = context.getJSONObject("appliances");
		rooms = context.getJSONObject("rooms");
		sensors = context.getJSONObject("agents").getJSONObject("sensors");
		//System.out.println(appliances.toString());
		//System.out.println(rooms.toString());
		//System.out.println(sensors.toString());
		
		//(deftemplate appliance 
		//	(slot id  (type String))
		//	(slot location  (type String))
		//	(slot state (type String)) 
		//)
		String appTemplate = "(appliance (id [id]) (location [location]) (state [state]))";
		for (String appid : JSONObject.getNames(appliances)) {
			String tmp = appTemplate;
			JSONObject app = appliances.getJSONObject(appid);
			tmp = tmp.replace("[id]", appid.toLowerCase());
			tmp = tmp.replace("[location]", app.getString("location").toLowerCase());
			tmp = tmp.replace("[state]", (sensors.getJSONObject((String) app.get("sensor"))).getString("subject_state").toLowerCase());
			System.out.println(tmp);
			jrete.assertString(tmp);
		}
		//(deftemplate room 
		//	(slot id  (type String) (default null) )
		//	(slot peopleInRoom (type Integer) (default 0))
		//)
		String roomTemplate = "(room (id [id]) (peopleInRoom [people]))";
		for (String roomid : JSONObject.getNames(rooms)) {
			String tmp = roomTemplate;
			JSONObject room = rooms.getJSONObject(roomid);
			tmp = tmp.replace("[id]", roomid.toLowerCase());
			tmp = tmp.replace("[people]", ""+room.getInt("number_of_persons"));
			System.out.println(tmp);
			jrete.assertString(tmp);
		}
		jrete.reset();
		jrete.run();
		
	} catch (UnsupportedEncodingException e) {
		e.printStackTrace();
	} catch (IOException e1) {
		e1.printStackTrace();
	}  catch (JSONException e) {
		e.printStackTrace();
	}

}
	
	@Override
	public void step(SimState arg0) {
		
		/*
		
		
		try {
			//Inicialización
			this.rete.clear();
			
			//Cargar Planes. 
			this.rete.batch(System.getProperty("user.dir").toString()+"/smash/code/lsint/e4/smash/reasoning/jess/shutdownUnattended.clp");
		
			//TODO Conservar facts relevantes de la ejecución anterior. 
			
		
		
			//Parsea información del paso actual y añade nuevos facts al sistema experto.
			if(!this.addNewFacts()){
				//Ha habido un error! ya está loggeado en el método. 
				return;
			}
			
			//TODO Ejecutar el sistema experto. 
			this.rete.reset();
			this.rete.run();
			
			//TODO Recoger el feedback para el proxiumo paso.
			
			//TODO Recoger el resultado y enviarselo a planificador. 
			
			
		} catch (JessException e) {
			e.printStackTrace();
			log.warning("[reasoning]ERROR! Ha habido un error en la ejecución "
					+ "del sistema experto. El sistema no puede tomar acciones "
					+ "En este paso.\nEl error ha sido: "+e.toString());
			return;
		}
		
		*/
	}
	
	private boolean addNewFacts() {
		JSONObject appliances;
		JSONObject rooms;
		JSONObject sensors;
		 try {
			 appliances = this.context.getJSONObject("appliances");
			 rooms = this.context.getJSONObject("rooms");
			 sensors = this.context.getJSONObject("sensors");
			
			// Construyendo fact 
			//"(appliance (id [id]) (location [location]) (state [state]))"
			String appTemplate = "(appliance (id [id]) (location [location]) (state [state]))";
			for (String appid : JSONObject.getNames(appliances)) {
				String appFact = appTemplate;
				JSONObject app = appliances.getJSONObject(appid);
				appFact = appFact.replace("[id]", "'"+appid+"'");
				appFact = appFact.replace("[location]", "'"+app.getString("location")+"'");
				appFact = appFact.replace("[state]", "'"+(sensors.getJSONObject(
							(String) app.get("sensor"))).get("subject_state")+"'");
				System.out.println(appFact);
				//Añadimos Appliance Facts al sistema experto
				this.rete.assertString(appFact);
			}
			
			// Contruyendo facts (room (id [id]) (peopleInRoom [people]))
			String roomTemplate = "(room (id [id]) (peopleInRoom [people]))";
			for (String roomid : JSONObject.getNames(rooms)) {
				String roomFact = roomTemplate;
				JSONObject room = rooms.getJSONObject(roomid);
				roomFact = roomFact.replace("[id]", "'"+roomid+"'");
				roomFact = roomFact.replace("[people]", ""+room.getInt("number_of_persons"));
				System.out.println(roomFact);
				//Añadimos room facts al sistema experto
				this.rete.assertString(roomFact);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
			log.warning("[reasoning]ERROR! no se han podido añadir facts para el"
					+ " paso actual. El sistema no puede tomar acciones este turno."
					+ "\nEl error ha sido: "+e.toString());
			return false;
		} catch (JessException e) {
			e.printStackTrace();
			log.warning("[reasoning]ERROR! no se han podido añadir facts para el"
					+ " paso actual. El sistema no puede tomar acciones este turno."
					+ "\nEl error ha sido: "+e.toString());
			return false;
		}
		//Se han añadido los hechos correctamente. 
		return true;
	}

	// GETER & SETTERS
	public void setContext(JSONObject context) {
		this.context = context;
	}
	
	public	String getId() {
		return this.id;
	}
	
	

	
	
	
	//no sirve al final??
	void GenerateFacts() throws JSONException {
		//lista de las  appliances
		int i=1;
		String appliance= "appliance#"+i;
		JSONObject app;
		while(context.getJSONObject(appliance)!=null){
			app=context.getJSONObject(appliance);
			fact.put(appliance, app.get("type"));
			
			i++;
			appliance= "appliance#"+i;
		}
	}
	
	
	//no me queda claro la utilidad de estos... 
	void Clean(){
		order.remove("OFF");
		order.remove("ON");
		order.remove("STAND_BY");
	}
	
	public JSONObject getOrder(){
		return order;
	}
	
	void GenerateOrder(int room) throws JSONException{
		String key="room#"+room;
		JSONObject r=context.getJSONObject(key);
		JSONArray app=r.getJSONArray("appliances");
	
		int i=0;
		while(app.get(i)!=null)
		{order.accumulate("OFF",app.get(i));
		i++;
		}
	}
	
	void Rule1 () throws JSONException{
		int i=1;
		int room;
		String person="person#"+i;
		while(context.getJSONObject(person)!=null){
		JSONObject pers=context.getJSONObject(person);
		room=pers.getInt("location");
		
		GenerateOrder(room);
		
		i++;
		person="person#"+i;
			}
	}
}
