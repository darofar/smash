(deftemplate appliance 
    "Representa la información útil de un electrodoméstico en el sistema experto"
    (slot id)
    (slot location)
    (slot state) 
)
(deftemplate room 
    "Representa la información útil de una habitación en el sistema experto"
    (slot id )
    (slot peopleInRoom)
)
(deftemplate appUnattendedCount
    "Representa la cuenta de los apsos que ha permanecido un electrodoméstico desatendido" 
    (slot id)
    (slot unattended)
    (slot originalState)
    (slot hasBeenModified)
)
(deftemplate Order 
    "Es el resultado final del sistema experto, habrá tantas ordenes como sean necesarias para cumplir el plan."
    (slot appId  (type String) (default null) ) 
)


; FASE 0
(defrule initilization
    "Inicialización del sistema experto"
    (declare (salience 100))
    (initial-fact)
=>
    (printout t "Plan 'Shutdown unattended Appliances' - Inicio de ejecucion." crlf) 
    (printout t (facts))
    (watch facts)
)


; FASE 1
(defrule createUnattendedAppliances
    "Si hay un electrodomestico desatendido y no existía cuenta inicia la cuenta"
    (declare (salience 20))
    (room (id ?roomNumber) (peopleInRoom 0))
    (appliance (location ?roomNumber) (id ?appID) (state ?appState))
    (or (test (= on ?appState)) (test (= stand_by ?appState))) 
    (not (appUnattendedCount (id ?appID) ))
=>
    (printout t "Create unattended" crlf)
    (assert (appUnattendedCount (id ?appID) (unattended 1) (originalState ?appState) (hasBeenModified TRUE)))
)
(defrule increaseUnattendedCount
    "Si hay un electrodomestico desatendido y ya existía la cuenta aumenta en uno"
    (declare (salience 20))
    (room (id ?roomNumber) (peopleInRoom 0))
    (appliance  (id ?appID) (location ?roomNumber) (state ?appState))
    (or (test (= on ?appState)) (test (= stand_by ?appState)))
    ?count <- (appUnattendedCount (id ?appID) (unattended ?un) (hasBeenModified FALSE))
=>
    (printout t "Increase Unattended" crlf)
    (modify ?count (unattended (+ ?un 1)) (hasBeenModified TRUE)) 
)
(defrule resetAttendedAppliancesByPeople
    "Si un electrodoméstico esta siendo usado nos aseguramos de resetear la cuenta"
    (declare (salience 20))
    (room (id ?roomNumber) (peopleInRoom ?people))
    (test(> ?people 0) )
    (appliance (location ?roomNumber) (id ?appID))
    ?countToDelete <- (appUnattendedCount (id ?appID))
=>
    (printout t "Reset attended by people" crlf)
    (retract ?countToDelete)
)
(defrule resetAttendedAppliancesByState
    "Si un electrodoméstico esta siendo usado nos aseguramos de resetear la cuenta"
    (declare (salience 20))
    (appliance (id ?appID) (state off))
    ?countToDelete <- (appUnattendedCount (id ?appID))
=>
    (printout t "Reset attended by state" crlf)
    (retract ?countToDelete)
)
(defrule changeFase
    (declare (salience 15))
    ;(or ?app<- (appliance) ?room<-(room))
    (initial-fact)
=>
    (printout t crlf "************************************************************ CHANGE FASE ************************************************************" crlf)
    (undefrule createUnattendedAppliances)
    (undefrule increaseUnattendedCount)
    (undefrule resetAttendedAppliancesByPeople)
    (undefrule resetAttendedAppliancesByState)
    (facts)
)


; FASE 2
(defrule generateOrders
    "Si un electrodomestico ha superado la cuenta de pasos desatendido actuamos"
    (declare (salience 10))
    (appUnattendedCount (unattended ?un) (id ?appID) (originalState ?os))
    (test (and (>= ?un 3) (eq ?os on))) 
    (not (Order (appId ?appID)))
=>
    (printout t "Generate Order" crlf)
    (assert (Order (appId ?appID)))
)

(defrule generateOrdersAndRemove
    "Si un electrodomestico ha superado la cuenta de pasos desatendido actuamos"
    (declare (salience 10))
    ?count <- (appUnattendedCount (unattended ?un) (id ?appID) (originalState ?os))
    (or (test (and (>= ?un 3) (eq ?os stand_by))) (test (>= ?un 6)))
    (not (Order (appId ?appID)))
=>
    (printout t "Generate Order and remove" crlf)
    (assert (Order (appId ?appID)))
    (retract ?count)
)

(defrule results
    "Resultado final"
    (declare (salience 0))
    ?ini<-(initial-fact)
=>
    (printout t crlf "************************************************************* RESULTS *************************************************************" crlf)
    (printout t (facts))
    (halt)
)
/*
(defrule prueba
    "Si hay un electrodomestico desatendido y no existía cuenta inicia la cuenta"
    (declare (salience 90))
	(room (id ?roomNumber) (peopleInRoom 0))
    (appliance (id ?appID) (location ?roomNumber) (state ?appState))
    (or (test (= on ?appState)) (test (= stand_by ?appState))) 
    (not (appUnattendedCount (id ?appID) ))
=>
    (assert (appUnattendedCount (id ?appID) (unattended 1) (originalState ?appState))) 
)
*/
/*
(assert (initial-fact))
(assert (appliance (id appliance#01) (location room#0) (state stand_by)))
(assert (appliance (id appliance#02) (location room#0) (state on)))
(assert (appliance (id appliance#03) (location room#0) (state on)))
(assert (appliance (id appliance#04) (location room#0) (state off)))
(assert (appliance (id appliance#05) (location room#0) (state stand_by)))
(assert (appliance (id appliance#11) (location room#1) (state on)))
(assert (appliance (id appliance#12) (location room#1) (state on)))
(assert (appliance (id appliance#13) (location room#1) (state off)))
(assert (appliance (id appliance#14) (location room#1) (state on)))
(assert (appliance (id appliance#15) (location room#1) (state off)))
(assert (appliance (id appliance#21) (location room#2) (state off)))
(assert (appliance (id appliance#22) (location room#2) (state on)))
(assert (appliance (id appliance#23) (location room#2) (state off)))
(assert (appliance (id appliance#24) (location room#2) (state stand_by)))
(assert (appliance (id appliance#25) (location room#2) (state off)))
(assert (appliance (id appliance#31) (location room#3) (state off)))
(assert (appliance (id appliance#32) (location room#3) (state on)))
(assert (appliance (id appliance#33) (location room#3) (state stand_by)))
(assert (appliance (id appliance#34) (location room#3) (state on)))
(assert (appliance (id appliance#35) (location room#3) (state on)))
(assert (room (id room#0) (peopleInRoom 2)))
(assert (room (id room#1) (peopleInRoom 0)))
(assert (room (id room#2) (peopleInRoom 0)))
(assert (room (id room#3) (peopleInRoom 1)))
(assert (appUnattendedCount (id appliance#02) (unattended 4) (originalState stand_by)(hasBeenModified FALSE)))
(assert (appUnattendedCount (id appliance#14) (unattended 5) (originalState on) (hasBeenModified FALSE)))
(assert (appUnattendedCount (id appliance#22) (unattended 2) (originalState on) (hasBeenModified FALSE)))
(assert (appUnattendedCount (id appliance#23) (unattended 3) (originalState on) (hasBeenModified FALSE)))
(assert (appUnattendedCount (id appliance#35) (unattended 1) (originalState stand_by) (hasBeenModified FALSE)))
(run)
*/