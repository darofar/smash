package lsint.e4.smash.statistics;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.smarthome.Appliance;
import lsint.e4.smarthome.Room;
import lsint.e4.util.Chart;
import lsint.e4.util.SmashLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import sim.engine.SimState;
import sim.engine.Steppable;














import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * @author Danny (darofar@gmail.com)
 * @author Nayeli (nn.cases@alumnos.upm.es)
 */
public class StatsAgent implements Steppable {

	private static final long serialVersionUID = 5208096050645986449L;
	
	//Orden en el que se ejecuta este Steppable en la simulación.
	//Cuanto mas bajo antes se ejecuta. Ver documento SECUENCIA_EJECUCION.doc
	public static final int SEQUENCE_ORDER = 7;
	private JSONObject context;
	private static Logger log = SmashLogger.getLogger(SmashSimulation.class.getSimpleName());
	
	/**
	 * identificador único. 
	 */
	private String id;
	private static int counterID;
	
	
	
	
	public StatsAgent() {
		this.id = StatsAgent.generateID();
	}

	@Override
	public void step(SimState arg0) {
		this.createFile((SmashSimulation) arg0);
	}

	private void createFile(SmashSimulation simulation) {
		String simulationID = simulation.getId().replace("#", "_").toLowerCase();
		String location = StatsAgent.class.getProtectionDomain().getCodeSource().getLocation().getFile();
		File contextFilePath = new File(location.substring(0, location.indexOf("bin"))+ "misc/context/"+simulationID);
		String contextFileName = "/context_"+simulationID+ "_step"+simulation.schedule.getSteps()+".json";
		// si el directorio no existe, lo creo. 
		if (!contextFilePath.exists()) {
			if(contextFilePath.mkdirs()) {
				System.out.println("LOG_PATH creado en: "+contextFilePath.getAbsolutePath());  
			} 
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(this.context.toString());
		String prettyJsonString = gson.toJson(je);


		FileWriter file;
		try {
			file = new FileWriter(contextFilePath.getAbsolutePath()+"/"+contextFileName);
			try {
				file.write(prettyJsonString);
				log.info("Successfully Copied JSON Object to File...");
				log.fine("\nJSON Object: " + prettyJsonString);
			} catch (IOException e) {
				log.warning(e.toString());
	        } finally {
	            file.flush();
	            file.close();
	        }
		} catch (IOException e1) {
			log.warning(e1.getMessage());
		}
	}

	public void setContext(JSONObject context) {
		try {
			this.context = new JSONObject(new JSONTokener(context.toString()));
		} catch (JSONException e) {
			e.printStackTrace();
			this.context = context;
		}
	}
	
	public static String generateID() {
	    return "STATS#"+StatsAgent.counterID++;
	}
	public String getId() {
	    return id;
	}
	
	public JSONObject readJSON(String path ) throws IOException, JSONException{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		  String jsonRaw =  new String(encoded, "UTF8");
		JSONObject json = new JSONObject(jsonRaw);
		return json;
	}
	
	public static void main(String[] args) throws IOException, JSONException {
		String workingDir = System.getProperty("user.dir");
		   System.out.println("Current working directory : " + workingDir);
			JSONObject res = new StatsAgent().readJSON(System.getProperty("user.dir")+"/misc/context/simulation_0529214018/context_simulation_0529214018_step188.json");
			JSONObject o = res.getJSONObject("simulation");
			//System.out.println(o.getJSONObject("consumption").get("total"));
			
	}
	
	
	public void totalConsumptionTime (String simulationId, SmashSimulation simulation){
		ArrayList<Double> result = new ArrayList<Double>();
		try {
			int numSteps = (int) (SmashSimulation.LIFE_SPAN/SmashSimulation.MINS_PER_STEP);
			for(int i=0;i<numSteps;i++){
			JSONObject json = this.readJSON(System.getProperty("user.dir")+"/misc/context/simulation_"+simulationId+"/context_simulation_"+simulationId+"_step"+i+".json");
			double partial =json.getJSONObject("simulation").getJSONObject("consumption").getDouble("total");
			result.add(partial);
			//System.out.println(result.get(result.size()-1));
			}
			//System.out.println(result);

			new Chart("line").generateChart("line", result, simulation);
			
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void consumptionPerAppliance(String simulationId, SmashSimulation simulation){
		ArrayList<Double> fridge = new ArrayList<Double>();
		ArrayList<Double> light = new ArrayList<Double>();
		ArrayList<Double> therm = new ArrayList<Double>();
		ArrayList<Double> tv = new ArrayList<Double>();
		ArrayList<Double> appliance = new ArrayList<Double>();
		
		
		try {
			int numSteps = (int) (SmashSimulation.LIFE_SPAN/SmashSimulation.MINS_PER_STEP)-1;

			JSONObject json = this.readJSON(System.getProperty("user.dir")+"/misc/context/simulation_"+simulationId+"/context_simulation_"+simulationId+"_step"+numSteps+".json");
			JSONObject partial =json.getJSONObject("simulation").getJSONObject("consumption").getJSONObject("per_appliance");
			int total = json.getJSONObject("simulation").getJSONObject("consumption").getInt("total");
			
			Iterator<String> keys = partial.keys();
			ArrayList<String> appTotal = new ArrayList<>();
			
			while(keys.hasNext()){
				appTotal.add(keys.next());
				
			}
			//System.out.println("El array de keys es  "+ appTotal);
			

				for(Room r : SmashSimulation.rooms){
					

					for(Appliance a: r.getAppInRoom()){
						for(String key: appTotal){
						//String key = keys.next();
							//System.out.println("a.getid  "+a.getId()+" key "+key);
							if(a.getId().equals(key)){
						

						if(a.getClass().getName().equals("lsint.e4.smarthome.Fridge")){
							fridge.add(partial.getDouble(key));
					}else if(a.getClass().getName().equals("lsint.e4.smarthome.Thermostat")){
						therm.add(partial.getDouble(key));
					}else if(a.getClass().getName().equals("lsint.e4.smarthome.Television")){
						tv.add(partial.getDouble(key));
					}else if(a.getClass().getName().equals("lsint.e4.smarthome.Light")){
						light.add(partial.getDouble(key));
					}else if(a.getClass().getName().equals("lsint.e4.smarthome.Appliance")){
						appliance.add(partial.getDouble(key));
					}
					}
					}
				}
			}
				//result.add(partial);
			//System.out.println(result.get(result.size()-1));
			
			System.out.println(light);
			new Chart("pie").pieConsumePeAppl(fridge, tv, light, therm, appliance, simulation, total);
			//new Chart("bar").barWithTeams(fridge, tv, light, therm, appliance, simulation);
			
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void appVsTotalConsumption (String simulationId, SmashSimulation simulation, String appClassName){
		
		ArrayList<Double> appliance = new ArrayList<Double>();
		
		
		try {
			int numSteps = (int) (SmashSimulation.LIFE_SPAN/SmashSimulation.MINS_PER_STEP)-1;

			JSONObject json = this.readJSON(System.getProperty("user.dir")+"/misc/context/simulation_"+simulationId+"/context_simulation_"+simulationId+"_step"+numSteps+".json");
			JSONObject partial =json.getJSONObject("simulation").getJSONObject("consumption").getJSONObject("per_appliance");
			int total = json.getJSONObject("simulation").getJSONObject("consumption").getInt("total");
			
			Iterator<String> keys = partial.keys();
			ArrayList<String> appTotal = new ArrayList<>();
			
			while(keys.hasNext()){
				appTotal.add(keys.next());
				
			}
			//System.out.println("El array de keys es  "+ appTotal);
			

				for(Room r : SmashSimulation.rooms){
					

					for(Appliance a: r.getAppInRoom()){
						for(String key: appTotal){
						//String key = keys.next();
							//System.out.println("a.getid  "+a.getId()+" key "+key);
							if(a.getId().equals(key)){					

						if(a.getClass().getName().equals("lsint.e4.smarthome."+appClassName)){
						appliance.add(partial.getDouble(key));
					}
					}
					}
				}
			}
				
			new Chart("bar").barTwoTeams(appliance, total, simulation, appClassName);
			
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
