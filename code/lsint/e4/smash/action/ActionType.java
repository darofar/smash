package lsint.e4.smash.action;

/**
 * Para futuras implementaciones de diferentes tipos de acción.  
 * @author Danny
 * @version v0.1.0
 */
public enum ActionType {
	CHANGE_STATE
}
