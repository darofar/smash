package lsint.e4.smash.action;
/**
 * Resultado de la acción. Los agentes Actuator modificarán este campo para
 * reflejar el resultado de la acción. 
 * 
 * PENDING: Aún no se ha realizado. 
 * SUCCESS: Se ha ejecutado con Éxito. 
 * FAIL: 	Ha sido imposible llevar la acción a cabo.
 * 
 *  @author Danny
 *  @version 0.1.0
 *  @see lsint.e4.smash.action.Action
 */
public enum Result {
	SUCCESS,
	FAIL,
	PENDING
}
