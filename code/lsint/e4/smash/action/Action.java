package lsint.e4.smash.action;

import java.util.logging.Logger;

import lsint.e4.util.SmashLogger;

/**
 * Action es una clase accesorio que representa la información de una acción 
 * ejecutada por SMASH! sobre la simulación de la casa inteligente.
 * 
 * Las acciones son generadas por el agente Scheduler en base a los planes 
 * decidídos por el agente Reasoning y enviadas a los agentes Actuator que
 * corresponde.  
 * 
 * @author Danny ()
 * @version v0.1.0
 * @see lsint.e4.smash.action.SchedulerAgent
 * @see lsint.e4.smash.action.ActuatorAgent
 */
public class Action {
	
	//para llevar la cuenta de los identificadores. 
	private static int counter;
	
	@SuppressWarnings("unused")
	private static Logger log = SmashLogger.getLogger(Action.class.getSimpleName());
	
	/**
	 * Identificador Único de la acción en la simulación actual. 
	 */
	private String id;
	
	/**
	 * identificador del Appliance objetivo de la acción.
	 */
	private String target;
	
	/**
	 * Tipo de la acciÃ³n. 
	 */
	private ActionType action;
	
	/**
	 * (opcional) Valor de la acción. Algunas acciones como cambiar el estado
	 * requieren un valor que indica que datos han de emplearse en su ejecución.  
	 */
	private String value;
	
	/**
	 * Resultado de la acciÃ³n para proporcionar feedback al scheduler. 
	 */
	private Result result;
	
	public Action(String target, ActionType at, String value) {
		super();
		this.target = target;
		this.action = at;
		this.value = value;
		this.setResult(Result.PENDING);
		this.id = Action.generateID();
	}
	
	private static String generateID() {
		return "ACTION#"+Action.counter++;
	}
	
	public ActionType getActionType() {
		return action;
	}
	public String getId() {
		return id;
	}
	public String getTarget() {
		return target;
	}
	public String getValue() {
		return value;
	}
	public Result getResult() {
		return result;
	}
	
	public void setActionType(ActionType action) {
		this.action = action;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setResult(Result r) {
		this.result = r;
	}
	
}