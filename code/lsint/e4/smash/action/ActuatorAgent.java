package lsint.e4.smash.action;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Logger;

import lsint.e4.smarthome.Appliance;
import lsint.e4.smarthome.Appliance.ApplianceState;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
import sim.engine.Steppable;

/**
 * This class represents the behaviour of a driver agent in the action layer. 
 * As an actuator the agent have to modify the current state of the appliance, 
 * called his subject. In addition this agent has to be able to communicate the
 * result of this operation (SUCCESS, FAIL, PENDING) to the SchedulerAgent 
 * for feedback
 * 
 * The actuator agent will have a queue of orders, on each given step it will 
 * get the older item on the queue and will try to execute. Corresponding to a 
 * FIFO queue. 
 * Also the agent have to offer an API for the queue of orders, to provide the 
 * ability of cancel orders.
 *  
 * @author Danny (darofar@gmail.com)
 *
 */
public class ActuatorAgent implements Steppable {
	
	private static final long serialVersionUID = 989850844185581037L;
	
	//Orden en el que se ejecuta este Steppable en la simulación.
	//Cuanto mas bajo antes se ejecuta. Ver documento SECUENCIA_EJECUCION.doc
	public static final int SEQUENCE_ORDER = 2;
	
	private String id;
	private static int counterID;
	private Appliance subject;
	private LinkedList <Action> orders;
	private static Logger log = SmashLogger.getLogger(ActuatorAgent.class.getSimpleName());
	
	public ActuatorAgent(Appliance subject) {
		super();
		this.subject = subject;
		this.id = ActuatorAgent.generateID();
		this.orders = new LinkedList<Action>();
		log.fine(this.id+" - created!");
		subject.setAsociatedActuatorAgent(this);
	}
	
	private static String generateID() {
		return "ACTUATOR_AGENT#"+ActuatorAgent.counterID++;
	}
	
	//Comunnication with scheduler. 
	public void giveOrder(Action newOrder){
		this.orders.add(newOrder);
	}
	
	public void cancelOrder(Action cancelOrder){
		this.orders.remove(cancelOrder); 
	}
	
	//Communication with context. 
	public String getSubjectID() {
		return this.subject.getId();
	}
	
	public LinkedList <Action> getOrders() {
		return this.orders;
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public void step(SimState arg0) {
		
		//if is empty we do nothing. 
		if(orders.peek() == null){
			return;
		}
		
		Action currentAction = orders.poll();
		if(currentAction.getActionType() == ActionType.CHANGE_STATE){
			try{
				if(currentAction.getValue() == ApplianceState.OFF.toString()){
					this.subject.setState(ApplianceState.OFF);
				} else if(currentAction.getValue() == ApplianceState.ON.toString()){
					this.subject.setState(ApplianceState.ON);
				} else if(currentAction.getValue() == ApplianceState.STAND_BY.toString()){
					this.subject.setState(ApplianceState.STAND_BY);
				} else {
					//throw new Exception("");
				}
				currentAction.setResult(Result.SUCCESS);
			} catch (Exception e) {
				e.printStackTrace();
				currentAction.setResult(Result.FAIL);
			}
		} else {
			return;
		}
	}

	public ArrayList<String> getOrdersAsString() {
		ArrayList<String> ordersAsString = new ArrayList<String>();
		for (Action action : this.getOrders()) {
			ordersAsString.add(action.getId());
		}
		return ordersAsString;
	}
}
