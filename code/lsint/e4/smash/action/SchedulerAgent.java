package lsint.e4.smash.action;

import sim.engine.SimState;
import java.util.ArrayList;
import java.util.logging.Logger;

import lsint.e4.smash.action.Action;
import lsint.e4.smash.action.ActuatorAgent;
import lsint.e4.smash.action.Result;
import lsint.e4.util.SmashLogger;
import sim.engine.Steppable;


/**
 * @author Danny (darofar@gmail.com)
 * @author antoine ()
 */
public class SchedulerAgent implements Steppable {

	private static final long serialVersionUID = -1527442687200482356L;

	/* (non-Javadoc)
	 * @see sim.engine.Steppable#step(sim.engine.SimState)
	 */
	private String target;
	private String id;
	private static Logger log = SmashLogger.getLogger(SchedulerAgent.class.getSimpleName());
	
	
	//los dos tipos de ordenes
    private ArrayList<Action> orderspend=new ArrayList<Action>();
    private ArrayList<Action> orderinact= new ArrayList<Action>();
	
    //la lista de los actuadores
    private ArrayList<ActuatorAgent> actuators;
    
    public SchedulerAgent(ArrayList<ActuatorAgent> actuators){
    	this.actuators=actuators;
    	this.id=SchedulerAgent.generateID();
    	log.fine(this.id+" - created!");
    	
    }
    
    private static String generateID() {
		return "SCHEDULER_AGENT#"+0;
	}
    
    
	
	//Para recibir los ordenes del razonador
	public void Getorders(ArrayList<Action> orders){
			
		orderspend.addAll(orders);
		
	}


	//para averiguar si los ordenes son realizables: tienen que tener un target diferente sino se borra el primero(a priori mas antiguo)
	public void Check(){
		
		int taille = orderspend.size();
		int i=0;
		Action act;
		boolean containtarget=false;
		for (i=0;i<taille;i++){
			target =orderspend.get(0).getTarget();
			act=orderspend.get(0);
			orderspend.remove(0);
			containtarget=false;
			for(int j=0;j<orderspend.size();j++){
				if(target==orderspend.get(j).getTarget()){
					containtarget=true;
					break;
				}
			}

			if(containtarget){
				System.out.println("order "+act.getValue()+" fue anulado para el actuador "+target);


			}else{
				orderspend.add(act);
				}
			}
	}

	
	// para pasar los ordenes pendientes a in action si se puede(tiene que el orden anterior para la misma target sea borrado)
	public void Passinaction(){
		int taille = orderspend.size();
		int i=0;
		Action act;
		boolean containtarget=false;
		for (i=0;i<taille;i++){
			target =orderspend.get(0).getTarget();
			act=orderspend.get(0);
			orderspend.remove(0);
			containtarget=false;
			for(int j=0;j<orderinact.size();j++){
				if(target==orderinact.get(j).getTarget()){
					containtarget=true;
					break;
				}
			}

			if(containtarget){
				orderspend.add(act);


			}else{
				orderinact.add(act);
				}
			}
	}

	//eliminar los ordenes ejecutados
	
	public void Clear(){
		for(int i=0;i<orderinact.size();i++){
			Action act = orderinact.get(0);
			target=act.getTarget();
			orderinact.remove(0);
			if(act.getResult()==Result.SUCCESS){
				System.out.println("action "+act.getActionType()+" para "+act.getTarget()+" ejecutado con exito");
				int j=0;
				for (j=0;j<actuators.size();j++){
					if( target == actuators.get(j).getSubjectID()){
					break;
					} 
				}
			    actuators.get(j).cancelOrder(act);
			}
			if(act.getResult()==Result.FAIL){
				System.out.println("action "+act.getActionType()+" para "+act.getTarget()+" no se ha logrado ejecutar");
				int j=0;
				for (j=0;j<actuators.size();j++){
					if( target == actuators.get(j).getSubjectID()){
					break;
					} 
				}
				actuators.get(j).cancelOrder(act);
			}
			if(act.getResult()==Result.PENDING){
				orderinact.add(act);
	
			}
		}
	}
	
	
	
	//enviar los ordenes a los actuadores:
	
	
	public void Send(){
		
		for(int i=0;i<orderinact.size();i++){
		target= orderinact.get(i).getTarget();
		int j=0;
			for (j=0;j<actuators.size();j++){
				if( target == actuators.get(j).getSubjectID()){
				break;
				} 
			}
		actuators.get(j).giveOrder(orderinact.get(i));
		
		}
		
	}
	
	//Se puede actuar sobre los orden pendientes(a priori no sirve)
	public void giveOrder(Action newOrder){
		this.orderspend.add(newOrder);
	}
	
	public void cancelOrder(Action cancelOrder){
		this.orderspend.remove(cancelOrder); 
	}
	
	
	
	
	
	@Override
	public void step(SimState arg0) {
		//corer los ordenes
		//Getorders(Reasoninagent.orders) el Reasoning puede ivocar este metodo al fin de su step,
		//el nombre no es mut bien elegido pero bueno
		//
		//Eliminar los inutiles
		Check();
		Clear();
		//Passar de pendientes a action
		Passinaction();
		//Enviar los ordenes
		Send();

	}

}