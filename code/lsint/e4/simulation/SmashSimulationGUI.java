package lsint.e4.simulation;

import java.awt.Color;

import javax.swing.JFrame;

import lsint.e4.smarthome.gui.GUISetup;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.grid.FastValueGridPortrayal2D;

public class SmashSimulationGUI extends GUIState {

    public Display2D display;
    public JFrame displayFrame;

   
    FastValueGridPortrayal2D skyPortrayal = new FastValueGridPortrayal2D("Sky");
    FastValueGridPortrayal2D housePortrayal = new FastValueGridPortrayal2D("House");
    FastValueGridPortrayal2D elementsPortrayal = new FastValueGridPortrayal2D("elements");
    

    public static void main(String[] args)
        {
        new SmashSimulationGUI().createController();
        }
    
    public SmashSimulationGUI() { super(new SmashSimulation(System.currentTimeMillis())); }
    
    public SmashSimulationGUI(SimState state) { super(state); }
    
    public static String getName() { return "***______SMASH_______***"; }
    
    
    public void quit()
        {
        super.quit();
        
        if (displayFrame!=null) displayFrame.dispose();
        displayFrame = null;  // let gc
        display = null;       // let gc
        }

    public void start()
        {
        super.start();
        // set up our portrayals
        setupPortrayals();
        }
    
    public void load(SimState state)
        {
        super.load(state);
        // we now have new grids.  Set up the portrayals to reflect that
        setupPortrayals();
        }
        
    // This is called by start() and by load() because they both had this code
    // so I didn't have to type it twice :-)
    public void setupPortrayals()
        {
        // tell the portrayals what to
        // portray and how to portray them
        
        housePortrayal.setField(
                GUISetup.House);
            housePortrayal.setMap(
                new sim.util.gui.SimpleColorMap(
                		new Color[] {new Color(0,0,0,0),Color.orange,Color.yellow, Color.black}));
            
            elementsPortrayal.setField(
                    GUISetup.electrics);
                elementsPortrayal.setMap(
                    new sim.util.gui.SimpleColorMap(
                    		new Color[] {new Color(0,0,0,0),Color.pink,Color.red, Color.blue,Color.green,Color.orange,Color.red}));
        skyPortrayal.setField(
                GUISetup.sky);
            skyPortrayal.setMap(
            		 new sim.util.gui.SimpleColorMap(
            	                0.0,1.0,Color.blue,Color.orange));
      
                   
        // reschedule the displayer
        display.reset();
                
        // redraw the display
        display.repaint();
        }
    
    public void init(Controller c)
        {
        super.init(c);
        
        // Make the Display2D.  We'll have it display stuff later.
        display = new Display2D(400,400,this); // at 400x400, we've got 4x4 per array position
        displayFrame = display.createFrame();
        c.registerFrame(displayFrame);   // register the frame so it appears in the "Display" list
        displayFrame.setVisible(true);

        // specify the backdrop color  -- what gets painted behind the displays
        //display.setBackdrop(Color.black);

        // attach the portrayals
        display.attach(skyPortrayal,"Sky");
        display.attach(housePortrayal,"House");
        display.attach(elementsPortrayal,"Elements");

        
        }
    }
    
    
     
    
    

