package lsint.e4.simulation;


import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import lsint.e4.smarthome.Appliance;
import lsint.e4.smarthome.Appliance.ApplianceState;
import lsint.e4.smarthome.Fridge;
import lsint.e4.smarthome.Ilumination;
import lsint.e4.smarthome.Light;
import lsint.e4.smarthome.Meter;
import lsint.e4.smarthome.Person;
import lsint.e4.smarthome.Room;
import lsint.e4.smarthome.Sensor;
import lsint.e4.smarthome.Television;
import lsint.e4.smarthome.Thermostat;
import lsint.e4.smarthome.gui.GUISetup;
import lsint.e4.smash.action.ActuatorAgent;
import lsint.e4.smash.action.SchedulerAgent;
import lsint.e4.smash.context.ContextualizatorAgent;
import lsint.e4.smash.reasoning.ReasoningAgent;
import lsint.e4.smash.sensor.SensorAgent;
import lsint.e4.smash.statistics.StatsAgent;
import lsint.e4.util.SmashLogger;
import sim.engine.SimState;
/**
 * 
 * 
 * @author Nayeli  (nn.cases@alumnos.upm.es)
 * @author Danny (rolando.fonseca.arboleda@alumnos.upm.es)
 * @author antoine (antoine.dussarps@supelec.fr)
 * 
 * @version v0.1.5
 */
public class SmashSimulation extends SimState {

	private static final long serialVersionUID = 2331204680603703125L;

	//Parámetros de la simulación.
	//identificador de simulación. 
	
	
	private String id;
	//El tiempo de ejecución de la simulación en minutos. 
	public static final long LIFE_SPAN = 1440;
	// Lo que equivale un step en tiempo simulado. 
	public static int MINS_PER_STEP;
	//properties file
	private static final String FILENAME = "config.properties";
	//Intervalo que se añade al current_time de la simulación a la hora de planificar Steppables. 
	private static final double EPSILON = 1.0; 
	//Logger	
	private static Logger log;
	//Tiempo en el que se creo la simulacion
	public static String currentTime;
	
	//Casa
	public static int NUMBER_OF_ROOMS;
	public static ArrayList<Room> rooms = new ArrayList<Room>();
	public static ArrayList<Person> people= new ArrayList<Person>();
	public static ArrayList<Appliance> app = new ArrayList<Appliance>();
	public Meter meter; 
	
	//Iluminación.
	public static double globalPercentajeIlumination=0;
	public static int auxIlum;
	public static double incrIlum;
	
	//agentes
	//Capa sensorial
	private ArrayList<SensorAgent> sensors = new ArrayList<SensorAgent>();
	//Capa de accion.
	private ArrayList<ActuatorAgent> actuators = new ArrayList<ActuatorAgent>();
	private SchedulerAgent scheduler;
	//Capa de contexto. 
	private ContextualizatorAgent contextualizator;
	//Capa de razonamiento
	private ReasoningAgent reasoner;
	//Capa de estadisticas
	private StatsAgent statistics;
	//Stepable que simula la luminosidad del día.
	private Ilumination ilumination;
	
	/**
	 * Metodo que lanza la simulación sin interfaz gráfica. 
	 * @param args
	 */
	public static void main(String[] args) {
		SmashSimulation simulation = new SmashSimulation(System.currentTimeMillis());
		simulation.start();
		log.log(Level.INFO, "[simulation] Starting simulation...");
		do{
			log.log(Level.INFO, "[simulation] Step: "+simulation.schedule.getSteps());
			if (!simulation.schedule.step(simulation)) { 
				break;
			}
		} while(simulation.schedule.getSteps()*SmashSimulation.MINS_PER_STEP < SmashSimulation.LIFE_SPAN);
		simulation.finish();
		log.log(Level.INFO, "[simulation] Simulation ended");
		System.exit(0);
	}  
	
	/**
	 * Construtor por defecto. Genera el ID de simulación e iniciliza el Logger.
	 * @param seed
	 */
	public SmashSimulation(long seed) {
		super(seed);
		this.id = SmashSimulation.generateID();
		SmashLogger.setup(this.id);
		log = SmashLogger.getLogger(SmashSimulation.class.getSimpleName());
		log.fine(this.id+" - created!");
	}
	
	/**
	 * Inicializa todos los elementos de la simulación parametrizandolos a 
	 * partir de el archivo de configuración config.properties.
	 * 
	 * Añade los "Stepables" al schedule de MASON.   
	 */
	public void start() {
		super.start();
		log.log(Level.INFO, "[startup]Starting configuration setup");
		
		//Carga configuracion
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = this.getClass().getClassLoader().getResourceAsStream(SmashSimulation.FILENAME);
			if(input==null){
				log.warning("Sorry, unable to find " + SmashSimulation.FILENAME+"\nFinishing Simulation");
				System.exit(1);
			}
			
			//load a properties file from class path, inside static method
			prop.load(input);
			log.log(Level.FINE, "[startup]Loaded config file");
			
			//Step
			SmashSimulation.MINS_PER_STEP = Integer.parseInt(prop.getProperty("step"));
			log.log(Level.FINE, "[startup]Step information: "+SmashSimulation.MINS_PER_STEP+" mins per step");
			
			//Primero electrodomesticos
			log.log(Level.FINE, "[startup]Appliance information");
			double off = Double.parseDouble(prop.getProperty("off"));
			
			double onTherm = Double.parseDouble(prop.getProperty("thermostatOn"));
			double stanByTherm = Double.parseDouble(prop.getProperty("thermostatStanBy"));

			double onFridge = Double.parseDouble(prop.getProperty("fridgeOn"));
			double stanByFridge = Double.parseDouble(prop.getProperty("fridgeStanBy"));

			double onTV = Double.parseDouble(prop.getProperty("TVOn"));
			double stanByTV = Double.parseDouble(prop.getProperty("TVStanBy"));

			double onLight = Double.parseDouble(prop.getProperty("lightOn"));
			double stanByLight= Double.parseDouble(prop.getProperty("lightStanBy"));

			double onApp = Double.parseDouble(prop.getProperty("applianceOn"));
			double stanByApp = Double.parseDouble(prop.getProperty("applianceStanBy"));
			
			//Segundo habitaciones
			log.log(Level.FINE, "[startup]Home information");
			int number_of_rooms = Integer.parseInt(prop.getProperty("rooms"));
			//System.out.println(number_of_rooms);
			SmashSimulation.NUMBER_OF_ROOMS = number_of_rooms;
			String beforeSplit = null;
			String[] result = null;
			
			log.log(Level.INFO, "[startup]Creating Rooms & Appliances");
			for(int i=0;i<number_of_rooms; i++){
				beforeSplit= prop.getProperty("room"+i).replaceAll("\\s","");
				result = beforeSplit.split(",");
				for(String x : result){
					//x = x.trim();
					if ("thermostat".equals(x)) {
						app.add(new Thermostat(ApplianceState.OFF, onTherm, off, stanByTherm,0));
					} else if ("light".equals(x)) {
						app.add(new Light(ApplianceState.OFF, onLight, off, stanByLight,0));
						//System.out.println(app.get(app.size()-1).getId());
					} else if ("tv".equals(x)) {
						app.add(new Television(ApplianceState.OFF, onTV, off, stanByTV,0));
					} else if ("fridge".equals(x)) {
						app.add(new Fridge(ApplianceState.OFF, onFridge, off, stanByFridge,0));
					} else if ("app".equals(x)) {
						app.add(new Appliance(ApplianceState.OFF, onApp, off, stanByApp,0, Appliance.class));
					} else if ("sensor".equals(x)) {
						app.add(new Sensor(ApplianceState.ON, 0, off, 0, 0));
					}
				}	
				rooms.add(new Room(i, app));
				app = new ArrayList<Appliance>();
				result=null;
				beforeSplit=null;
			}	
			
			//Tercero personas
			log.log(Level.INFO, "[startup]Creating people agents");
			int number_of_people = Integer.parseInt(prop.getProperty("people"));

			for(int s=0;s<number_of_people;s++){
				String beforeSplitP= prop.getProperty("person"+s);
				String[] resultP = beforeSplitP.split(",");
				Person tmperson = new Person(Double.parseDouble(resultP[4]), 
						Double.parseDouble(resultP[0]), Double.parseDouble(resultP[3]), 
						Double.parseDouble(resultP[2]), Double.parseDouble(resultP[1]),  
						SmashSimulation.rooms.get(0));
				people.add(tmperson);
				SmashSimulation.rooms.get(0).addPeopleInRoom(tmperson);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally{
			if(input!=null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		//Inicia el meter
		log.log(Level.INFO, "[startup] Creating Meter");
		this.meter = new Meter();
		this.meter.init();
		
		//Inicia la iluminación 
		this.ilumination = new Ilumination();
		
		//Creating agents. 
		log.log(Level.FINE, "[startup] Creating SMASH! agents");
		for (Room r : SmashSimulation.rooms) {
			for (Appliance app : r.getAppInRoom()) {
				this.getActuators().add(new ActuatorAgent(app));
				this.getSensors().add(new SensorAgent(app));
			}
		}
		this.setScheduler(new SchedulerAgent(this.getActuators()));
		this.setReasoner(new ReasoningAgent());
		this.setStatistics(new StatsAgent());
		this.setContextualizator(new ContextualizatorAgent(
			this.getReasoner(),
			this.getStatistics(),
			this.getScheduler(), 
			this.getSensors(),
			this.getActuators(), 
			this.meter
		));
		
		log.info("[startup] Scheduling Stepables");
		/* [SECUENCIA]1
		 * Eventos. En nuestra simulación no habrá, salvo el cambio de hora,
		 * pero como va intrínsecamente ligado al número de paso, tampoco lo 
		 * resolveremos como un evento. De haber, aquí ocurrirían los errores, 
		 * cambios del clima, etc…
		 */
		//Iluminacion
		schedule.scheduleRepeating(this.ilumination, Ilumination.SEQUENCE_ORDER, SmashSimulation.EPSILON);
		log.log(Level.FINE, "[startup] sequence[1] : "+this.ilumination.getId());
		
		/* [SECUENCIA]2
		 *  Actuadores. Todos los actuadores ejecutan una de las acciones que 
		 *  tengan en cola.
		 */
		String actuatorIDs = "[";
		for (ActuatorAgent act : this.actuators) {
			this.schedule.scheduleRepeating(act, ActuatorAgent.SEQUENCE_ORDER, SmashSimulation.EPSILON);
			actuatorIDs+=act.getId()+",";
		}
		actuatorIDs+="]";
		log.fine("[startup] sequence[2]: "+actuatorIDs);
		
		/* [SECUENCIA]3
		 * Personas. Todas las personas ejecutan un paso de su comportamiento.
		 */
		String peopleIDs = "["; 
		for(int i=0; i< SmashSimulation.people.size(); i++){
			this.schedule.scheduleRepeating(SmashSimulation.people.get(i), Person.SEQUENCE_ORDER, SmashSimulation.EPSILON);
			peopleIDs+= SmashSimulation.people.get(i)+",";
		}
		peopleIDs+="]";
		log.fine("[startup] sequence[3]: "+peopleIDs);
		
		/* [SECUENCIA]4
		 * Sensores. Los sensores actualizan en su buffer el estado del elemento
		 * que vigilan.
		 */
		String sensorsIDs = "[";
		for (SensorAgent sen : this.sensors) {
			this.schedule.scheduleRepeating(sen, SensorAgent.SEQUENCE_ORDER, SmashSimulation.EPSILON);
			sensorsIDs+=sen.getId()+",";
		}
		sensorsIDs+="]";
		log.fine("[startup] sequence[4]: "+sensorsIDs);
		
		/* [SECUENCIA]5
		 * Contador: El contador registra el consumo de todos los agentes.
		 */
		this.schedule.scheduleRepeating(this.meter, Meter.SEQUENCE_ORDER, SmashSimulation.EPSILON);
		log.fine("[startup] sequence[5]: "+this.meter.getId());
		
		/* [SECUENCIA]6
		 * Contextualizador: recorre todos los agentes recabando información.
		 * Crea el contexto JSON y se pasa a estadísticas y razonador.
		 */
		this.schedule.scheduleRepeating(this.contextualizator, ContextualizatorAgent.SEQUENCE_ORDER, SmashSimulation.EPSILON);
		log.fine("[startup] sequence[6]: "+this.contextualizator.getId());
		
		/* [SECUENCIA]7
		 * Stats: (otro hilo?) guarda el contexto.
		 */
		this.schedule.scheduleRepeating(this.statistics, StatsAgent.SEQUENCE_ORDER, SmashSimulation.EPSILON);
		log.fine("[startup] sequence[7]: "+this.statistics.getId());

		/* [SECUENCIA]8
		 * Razonador: Lanza el sistema experto en base al contexto. Tras el 
		 * cálculo del sistema experto envía las órdenes al planificador.
		 */
		//TODO
		
		/* [SECUENCIA]9
		 * Planificador: Comprueba su cola de órdenes y calcula el plan a seguir. 
		 * Ordena su cola de ordenes pro prioridad. Envía las órdenes más 
		 * prioritarias a los actuadores.
		 */ 
		//TODO
		
		
		//Logging startup summary
		int appliancesCount = 0;
		for (Room r : SmashSimulation.rooms) {
			appliancesCount+=r.getAppInRoom().size();
		}
		log.info("[startup] Initialization Summary: \n"+
		"Number of rooms: "+SmashSimulation.rooms.size()+"\n"+
		"Number of people: "+SmashSimulation.people.size()+"\n"+
		"Number of appliances: "+appliancesCount+"\n"+
		"Number of actuators: "+this.actuators.size()+"\n"+
		"Number of sensors: "+this.sensors.size()+"\n"
		);
		GUISetup.setup(this);
	}

	@Override
	public void finish() {
		super.finish();
		//TODO aquí van los calculos de las estadísticas. 
		log.info(this.meter.totalConsumptionPerAppliance.toString());
		this.statistics.totalConsumptionTime(SmashSimulation.currentTime, this);
		this.statistics.consumptionPerAppliance(SmashSimulation.currentTime, this);
		this.statistics.appVsTotalConsumption(SmashSimulation.currentTime, this, "Light");
		this.statistics.appVsTotalConsumption(SmashSimulation.currentTime, this, "Fridge");
		this.statistics.appVsTotalConsumption(SmashSimulation.currentTime, this, "Television");
		this.statistics.appVsTotalConsumption(SmashSimulation.currentTime, this, "Thermostat");
		this.statistics.appVsTotalConsumption(SmashSimulation.currentTime, this, "Appliance");
		//System.out.println("El id de la simulacion es:  "+ SmashSimulation.currentTime);
	}
	
	/** ACCESORY METHODS */
	
	/**
	 * SIMULATION#TIMESTAMP() ejemplo: SIMULATION#21052014122235
	 * @return
	 */
	public static String generateID() {
		SmashSimulation.currentTime = new SimpleDateFormat("MMddHHmmss")
		.format(new java.sql.Timestamp(
				Calendar.getInstance().getTime().getTime()));
		return "SIMULATION#"+SmashSimulation.currentTime;
	}
	public String getId() {
	    return id;
	}
	public static double getGlobalPercentajeIlumination() {
		return globalPercentajeIlumination;
	}
	public static void setGlobalPercentajeIlumination(
			double globalPercentajeIlumination) {
		SmashSimulation.globalPercentajeIlumination = globalPercentajeIlumination;
	}
	
	//Agents Getter & Setters
	public ArrayList<SensorAgent> getSensors() {
		return sensors;
	}
	public void setSensors(ArrayList<SensorAgent> sensors) {
		this.sensors = sensors;
	}
	public ArrayList<ActuatorAgent> getActuators() {
		return actuators;
	}
	public void setActuators(ArrayList<ActuatorAgent> actuators) {
		this.actuators = actuators;
	}
	public SchedulerAgent getScheduler() {
		return scheduler;
	}
	public void setScheduler(SchedulerAgent scheduler) {
		this.scheduler = scheduler;
	}
	public ContextualizatorAgent getContextualizator() {
		return contextualizator;
	}
	public void setContextualizator(ContextualizatorAgent contextualizator) {
		this.contextualizator = contextualizator;
	}
	public ReasoningAgent getReasoner() {
		return reasoner;
	}
	public void setReasoner(ReasoningAgent reasoner) {
		this.reasoner = reasoner;
	}
	public StatsAgent getStatistics() {
		return statistics;
	}
	public void setStatistics(StatsAgent statistics) {
		this.statistics = statistics;
	}
}
