package lsint.e4.util;

import static com.googlecode.charts4j.Color.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import lsint.e4.simulation.SmashSimulation;
import lsint.e4.smash.statistics.StatsAgent;

import com.googlecode.charts4j.*;




/**
 * Clase auxiliar para generar las gráficas de rendimiento del sistema.
 * @author toffeshop
 *
 */
public class Chart {
	
	private String id;
	
	private static int counterID;
	private String type;
	
	/**
	 * Lista de los tipos de gráficas disponibles
	 */
	private static ArrayList<String> types = new ArrayList<String>(Arrays.asList("bar", "line", "scatter", "xy", "radar"));
	private static Logger log = SmashLogger.getLogger(Chart.class.getSimpleName());

	
	public Chart(String type) {
		super();
		this.id = Chart.generateID();
		this.type = type;
	}

	
	
	 private String lineChart(ArrayList<Double> data) {
	        String result;
	        ArrayList<Double> data1 = resizeData(data);
	       // List<Double> list = data;
	         Data list1 = Data.newData(data1);
	        // Data list2 = Data.newData(data2);
	         Line line = Plots.newLine(DataUtil.scaleWithinRange(0,100,data1), YELLOW, "Consumption");
			 //Plot plot = Plots.newPlot(list);
	         line.addShapeMarkers(Shape.CIRCLE, YELLOW, 10);
	         line.addShapeMarkers(Shape.CIRCLE, BLACK, 2);
		     //plot.addShapeMarkers(Shape.DIAMOND, BLUE, 12);
		     
	         LineChart chart = GCharts.newLineChart(line);
	         AxisStyle axisStyle = AxisStyle.newAxisStyle(WHITE, 12, AxisTextAlignment.CENTER);
	         
	         AxisLabels xAxis2 = AxisLabelsFactory.newNumericRangeAxisLabels(data.get(0), data.get(data.size()-1));
	         xAxis2.setAxisStyle(axisStyle);
	         chart.addYAxisLabels(xAxis2);

	         chart.setGrid(25, 25, 3, 2);
	         chart.setSize(600, 450);
	         chart.setBackgroundFill(Fills.newSolidFill(Color.newColor("1F1D1D")));
	         LinearGradientFill fill = Fills.newLinearGradientFill(0, Color.newColor("363433"), 100);
	         fill.addColorAndOffset(Color.newColor("2E2B2A"), 0);
	         chart.setAreaFill(fill);
	        result = chart.toURLString();
	        System.out.println(result);
	        return result;
	    }
	 
	 private String barChart(ArrayList<Double> data) {
		 String result="";
		 
		 List<Double> list = data;
		 Plot plot = Plots.newPlot(Data.newData(list));
	     plot.addShapeMarkers(Shape.DIAMOND, BLUE, 12);
	     /*
		 BarChartchart = GCharts.newBarChart(plot);
	     barChart.setSize(400, 200);
	     result = barChart.toURLString();   
	        */
	     return result;
	    }
	 
	 private String radarChart(ArrayList<Double> data) {
		 String result;
		 
		 List<Double> list = data;
		 Plot plot = Plots.newPlot(Data.newData(list));
	     plot.addShapeMarkers(Shape.DIAMOND, BLUE, 12);
	     
		 RadarChart radarChart = GCharts.newRadarChart(plot);
	     radarChart.setSize(400, 200);
	     result = radarChart.toURLString();
	     
	     return result;
	    }
	 
	 private String scatterChart(ArrayList<Double> data) {
		 String result;
		 
		 List<Double> list = data;
		 Plot plot = Plots.newPlot(Data.newData(list));
	     plot.addShapeMarkers(Shape.DIAMOND, BLUE, 12);
	     
		 ScatterPlot  scatterChart = GCharts.newScatterPlot(plot);
	     scatterChart.setSize(400, 200);
	     result = scatterChart.toURLString();
	     
	     return result;
	    }
	 
	 private String XYLineChart(ArrayList<Double> data) {
		 String result;
		 
		 List<Double> list = data;
		 Plot plot = Plots.newPlot(Data.newData(list));
	     plot.addShapeMarkers(Shape.DIAMOND, BLUE, 12);
	     
		XYLineChart  xyLineChart = GCharts.newXYLineChart(plot);
	    xyLineChart.setSize(400, 200);
	    result = xyLineChart.toURLString();
	    
	    return result;
	    }
	 
	 	private static String generateID() {
			return "CHART#"+Chart.counterID++;
		}
	 	
	 	public String pieConsumePeAppl(ArrayList<Double> fridge,ArrayList<Double> tv,ArrayList<Double> light,ArrayList<Double> therm,ArrayList<Double> app, SmashSimulation simulation, double total){
	 		
	 		double fridgeTot=0;
	 		for(int i = 0; i < fridge.size(); i++)
	 		    fridgeTot += fridge.get(i);
	 		double tvTot=0;
	 		for(int i = 0; i < tv.size(); i++)
	 		    tvTot += tv.get(i);
	 		
	 		double lightTot=0;
	 		
	 		//System.out.println("light size   "+light.get(0));

	 		for(int i = 0; i < light.size(); i++)
	 		    lightTot += light.get(i);
	 		
	 		double thermTot=0;
	 		for(int i = 0; i < therm.size(); i++)
	 		    thermTot += therm.get(i);
	 		
	 		double appTot=0;
	 		for(int i = 0; i < app.size(); i++)
	 		    appTot += app.get(i);
	 		
	 		//System.out.println("light   "+lightTot);
	 		
	 		
	 		 Slice fridgePer = Slice.newSlice(getPercetage(total, fridgeTot), Color.newColor("CACACA"), "Fridge");
	         Slice tvPer = Slice.newSlice(getPercetage(total, tvTot), Color.newColor("DF7417"), "TV");
	         Slice lightPer = Slice.newSlice(getPercetage(total, lightTot), Color.newColor("951800"), "Lights");
	         Slice thermPer = Slice.newSlice(getPercetage(total, thermTot), Color.newColor("01A1DB"), "Thermostat");
	         Slice appPer = Slice.newSlice(getPercetage(total, appTot), Color.newColor("F7FE2E"), "General appliance");
	         
	         PieChart chart = GCharts.newPieChart(fridgePer, tvPer, lightPer, thermPer, appPer);
	        // chart.setTitle("A Better Web", BLACK, 16);
	         chart.setSize(500, 200);
	         chart.setThreeD(true);
	         String url = chart.toURLString();
	         
	         String finalUrl = writeToFile(url, simulation);
		        
		     System.out.println(finalUrl);
		     return finalUrl;
	 	}
	 	
	 	private int getPercetage(double total, double partial){
	 		return (int) ((partial*100)/total);
	 	}
	 	
	 public String barTwoTeams(ArrayList<Double> app,double total, SmashSimulation simulation, String name){
		 	
		 double appTot=0;
	 		for(int i = 0; i < app.size(); i++)
	 		    appTot += app.get(i);
		 
		 	ArrayList<Double> aux= new ArrayList<>();
		 	aux.add(appTot);
		 	aux.add(total);
		 	
		 	aux = resizeData(aux);
		 	
		 	
		 
		 	
		 	
		 	
	        BarChartPlot team1 = Plots.newBarChartPlot(Data.newData(aux.get(0), 0), BLUEVIOLET, name);
	        BarChartPlot team2 = Plots.newBarChartPlot(Data.newData(0, aux.get(1)), ORANGERED, "Total");
	       // BarChartPlot team3 = Plots.newBarChartPlot(data3, LIMEGREEN, "Light");
	        //BarChartPlot team4 = Plots.newBarChartPlot(data4, RED, "Thermostat");
	        //BarChartPlot team5 = Plots.newBarChartPlot(data5, YELLOW, "Appliance");

	        

	        // Instantiating chart.
	        BarChart chart = GCharts.newBarChart(team1, team2);

	        // Defining axis info and styles
	        AxisStyle axisStyle = AxisStyle.newAxisStyle(BLACK, 13, AxisTextAlignment.CENTER);
	       // AxisLabels score = AxisLabelsFactory.newAxisLabels("Score", 50.0);
	        //score.setAxisStyle(axisStyle);
	        //AxisLabels year = AxisLabelsFactory.newAxisLabels("Year", 50.0);
	        //year.setAxisStyle(axisStyle);

	        // Adding axis info to chart.
	        //chart.addXAxisLabels(AxisLabelsFactory.newAxisLabels("2002", "2003", "2004", "2005"));
	        chart.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, total));
	       // chart.addYAxisLabels(score);
	       // chart.addXAxisLabels(year);

	        chart.setSize(600, 450);
	        chart.setBarWidth(50);
	        chart.setSpaceWithinGroupsOfBars(30);
	        chart.setDataStacked(true);
	        //chart.setTitle("Team Scores", BLACK, 16);
	        chart.setGrid(100, 10, 3, 2);
	        //chart.setBackgroundFill(Fills.newSolidFill(ALICEBLUE));
	       LinearGradientFill fill = Fills.newLinearGradientFill(0, LAVENDER, 100);
	       fill.addColorAndOffset(WHITE, 0);
	        chart.setAreaFill(fill);
	        String url = chart.toURLString();
	        
	        String finalUrl = writeToFile(url, simulation);
	        
	        System.out.println(finalUrl);
	        return finalUrl;
	 }
	 
	 
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
		
		
		/**
		 * Genera a partir de los datos proporcionados y el tipo un String que es 
		 * una url a una grafica de google.
		 * 
		 * Se guarda en ~/misc/charts/
		 * 
		 * @param type: tipo de grafica
		 * @param data: datos utilizados
		 * @param simulation: simulacion en la que se trabaja
		 * @return
		 */
		public String generateChart(String type, ArrayList<Double> result, SmashSimulation simulation){
			String url="";
			//ArrayList<Double> result = resizeData(res);
			
			//System.out.println(result);
			for(String r : Chart.types){
				if(type.equals(r)){
					if("bar".equals(r)){
						url = new Chart(type).barChart(result);
					}else if("line".equals(r)){
						url = new Chart(type).lineChart(result);

					}else if("scatter".equals(r)){
						url = new Chart(type).scatterChart(result);

					}else if("xy".equals(r)){
						url = new Chart(type).XYLineChart(result);

					}else if("radar".equals(r)){
						url = new Chart(type).radarChart(result);

					}
				}
			}
			
			String finalURL = writeToFile(url, simulation);
			
			
			return finalURL;
		}
		
		private String writeToFile(String url, SmashSimulation simulation){
			String simulationID = simulation.getId().replace("#", "_").toLowerCase();
			String location = StatsAgent.class.getProtectionDomain().getCodeSource().getLocation().getFile();
			File chartFilePath = new File(location.substring(0, location.indexOf("bin"))+ "misc/charts/"+simulationID);
			String chartFileName = "/chart_"+simulation.getId()+ "_"+this.getType()+"_id"+this.getId();

			// si el directorio no existe, lo creo. 
			if (!chartFilePath.exists()) {
				if(chartFilePath.mkdirs()) {
					System.out.println("LOG_PATH creado en: "+chartFilePath.getAbsolutePath());  
				} 
			}
			
			FileWriter file;
			try {
				file = new FileWriter(chartFilePath.getAbsolutePath()+"/"+chartFileName);
				try {
					file.write(url);
					log.info("Successfully Copied chart url to File...");
					
				} catch (IOException e) {
					log.warning(e.toString());
		        } finally {
		            file.flush();
		            file.close();
		        }
			} catch (IOException e1) {
				log.warning(e1.getMessage());
			}
			
			return url;
		}

	public static void main(String[] args) {
		 ArrayList<Double> re = new ArrayList<>();
		 re.add(70.0);
		 re.add(10.0);
		 re.add(20.0);
		 re.add(30.0);
		 re.add(40.0);
		 Plot plot = Plots.newPlot(Data.newData(re));
	     plot.addShapeMarkers(Shape.DIAMOND, BLUE, 12);
	     
        LineChart lineChart = GCharts.newLineChart(plot);
        lineChart.setSize(500, 500);	
        String result = lineChart.toURLString();
        System.out.println(result);
	}



	public static ArrayList<String> getTypes() {
		return types;
	}



	public static void setTypes(ArrayList<String> types) {
		Chart.types = types;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}
	
	private ArrayList<Double> resizeData (ArrayList<Double> res){
		ArrayList<Double> result = new ArrayList<>();
		if(res.size()>0){
		Collections.sort(res); 
		double max = res.get(res.size()-1);
		
		for(int i=0;i<res.size();i++){
			result.add(i, (100*res.get(i))/max);
		}
		return result;
		}else{
			return res;
		}
		
	}
	
	

}
