package lsint.e4.util;

import java.io.File;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author dfa
 *
 */
public class SmashLogger {

	private static File logpath;
	private static final Level fileLogLevel = Level.ALL;
	private static final Level consoleLogLevel = Level.CONFIG;
	static public void setup(String simulationID) {

		//Creando el path a los logs
		String location = SmashLogger.class.getProtectionDomain()
							.getCodeSource().getLocation().getFile();
		logpath = new File(location.substring(0, location.indexOf("bin"))
							+"logs/"+simulationID.replace("#", "_").toLowerCase());
		
		// si el directorio no existe, lo creo. 
		if (!logpath.exists()) {
			if(logpath.mkdirs()) {
				System.out.println("LOG_PATH creado en: "+logpath.getAbsolutePath());  
			} 
		}
	}
	
	public static Logger getLogger(String ClassName){
		Logger log = Logger.getLogger(ClassName);
		FileHandler logFileHandler = null;
		try {
			logFileHandler = new FileHandler(logpath.getAbsolutePath()+"/"+ClassName+".log");
			logFileHandler.setFormatter(new SimpleFormatter());
			logFileHandler.setLevel(SmashLogger.fileLogLevel);
			log.addHandler(logFileHandler);
		} catch (SecurityException e) {
			logFileHandler = null;
			e.printStackTrace();
		} catch (IOException e) {
			logFileHandler = null;
			e.printStackTrace();
		} catch (Exception e) {
			logFileHandler = null;
			e.printStackTrace();
		}
		if(logFileHandler == null){
			System.out.println("Imposible crear el archivo de log. Logging en consola.");
		}
		log.setLevel(SmashLogger.fileLogLevel);
		return log;
	}
	
	public static void main(String[] args) {
		SmashLogger.setup("prueba");
		Logger log = SmashLogger.getLogger(SmashLogger.class.getSimpleName());
		for (Handler h : log.getHandlers()) {
			System.out.println(h.toString());
			System.out.println(h.getClass());
		}
		
	}
}
